VERSION=$1

PACKAGE_JSON_VERSION=$(sed -n -E 's/\s*"version"\s*:\s*"(.*)",/\1/p' frontend/package.json)
cd backend
MVN_POM_VERSION=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec 2>/dev/null)
APP_PROPERTIES_VERSION=$(sed -nE 's/golden-agents-version:\s*(.*)/\1/p' src/main/resources/application.yml)

if [[ $MVN_POM_VERSION == $APP_PROPERTIES_VERSION && $MVN_POM_VERSION == $PACKAGE_JSON_VERSION ]]; then
    echo "Current software version is '$MVN_POM_VERSION'"
else
    echo "Inconsistent versions set:"
    echo -e "\tMaven pom.xml: \"${MVN_POM_VERSION}\""
    echo -e "\tApplication properties (application.yml): \"${APP_PROPERTIES_VERSION}\""
    echo -e "\tFrontend package.json: \"${PACKAGE_JSON_VERSION}\""
    echo "WARNING: All versions will be overwritten with the new specified value"
fi 

while [[ -z $VERSION ]]; do
    echo "Enter the new version to set for the backend software:"
    read VERSION
done

# Update version in pom.xml (backend)
echo "$VERSION" | mvn versions:set

cd ../

# Update version in application properties (backend)
sed -E -i "s/(golden-agents-version:).*/\1 ${VERSION}/g" backend/src/main/resources/application.yml
echo "Updated golden-agents-version in application.yml from '$APP_PROPERTIES_VERSION' to '$VERSION'"

# Update version in package.json (frontend)
sed -i -E "s/(version.: )\"(.*)\",/\1\"$VERSION\",/g" frontend/package.json
echo "Updated version in frontend package.json to from '$PACKAGE_JSON_VERSION' to '$VERSION'"




