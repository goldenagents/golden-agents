#!/bin/bash

FRONTEND=golden-agents-frontend
CONTAINER_ID_CMD="docker container ls -a --format="{{.ID}}" --filter name=${FRONTEND}"

# Build frontend (UI)
cd frontend
docker build --rm -t goldenagents/frontend .

# Remove frontend (if already present)
CONTAINER_ID=$($CONTAINER_ID_CMD)
if [[ -n $CONTAINER_ID ]]; then
    echo "CONTAINER $CONTAINER_ID already exists, removing"
    docker container stop $CONTAINER_ID
    docker container rm $CONTAINER_ID
fi

# Run frontend now that name is free
docker run -dp 4200:80 --env API_URL=http://localhost --env API_PORT=8080 --name goldenagents-frontend goldenagents/frontend

# Store frontend container ID and IP address for use
FE_ID=$($CONTAINER_ID_CMD)
IP="http://$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $FE_ID)"

# Build backend (agent infrastructure + API)
cd ../backend
docker build --rm -t goldenagents/backend .
docker run -dp 8080:8080 --cpus=8 --env ALLOWED_ORIGINS="http://localhost:4200,http:127.0.0.1:4200,$IP,$IP:80" goldenagents/backend

# Leave instructions
echo "Go to browser and open http://localhost:4200"
echo -e "\n\nOr access container directly via $IP"

