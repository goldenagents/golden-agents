The [Golden Agents](https://www.goldenagents.org/) framework is a multi-agent system created to assist humanities researchers to create queries and find their answers across multiple semantically heterogenous RDF graphs from the domain of cultural heritage.

The framework consists of two modules: The backend, which is where the agent technology lives, and the frontend, which is the user interface through which users interact with the agents.
## Running with IDE
Both modules should be opened in separate projects when used with an IDE. Both modules can also be built as two separate Docker images. Instructions for both can be found in the directories within this repository corresponding to the two modules.

## Testing setup
To quickly test the framework, the Docker images can easily be started with either docker-compose, or the utility script.

### docker-compose
Installation instructions for Docker and Docker Compose can be found here:
- https://docs.docker.com/engine/install/
- https://docs.docker.com/compose/install/
```bash
$ docker-compose up -d
```

### Utility script
A utility script, `rundocker.sh` is provided that builds, configures and runs the docker images of both modules for local use.
```bash
$ chmod +x rundocker.sh
$ ./rundocker.sh
```

## Utility
The version of the software can be incremented with the `bump-version.sh` shell script.