package org.uu.nl.goldenagents.agent.context;

import org.uu.nl.goldenagents.netmodels.angular.CrudAgent;
import org.uu.nl.net2apl.core.agent.Context;

public class UIContext implements Context {

	private String icon;
	private String nickname;
	private CrudAgent.AgentType type;
	private String description;
	private String homepage;
	
	public UIContext(String icon, String nickname, CrudAgent.AgentType type, String description, String homepage) {
		this.icon = icon;
		this.nickname = nickname;
		this.type = type;
		this.description = description;
		this.homepage = homepage;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public CrudAgent.AgentType getType() {
		return this.type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
}
