package org.uu.nl.goldenagents.agent.plan.broker.suggestions;

import org.apache.jena.atlas.lib.SetUtils;
import org.uu.nl.goldenagents.agent.context.BrokerContext;
import org.uu.nl.goldenagents.agent.context.BrokerSearchSuggestionsContext;
import org.uu.nl.goldenagents.agent.context.BrokerSearchSuggestionsContext.SearchSuggestion;
import org.uu.nl.goldenagents.agent.plan.MessagePlan;
import org.uu.nl.goldenagents.netmodels.AqlDbTypeSuggestionWrapper;
import org.uu.nl.goldenagents.netmodels.fipa.EntityList;
import org.uu.nl.goldenagents.netmodels.fipa.GAMessageContentWrapper;
import org.uu.nl.goldenagents.netmodels.fipa.GAMessageHeader;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.defaults.messenger.MessageReceiverNotFoundException;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.FIPASendableObject;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.fipa.acl.UnreadableException;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;
import org.uu.nl.net2apl.core.platform.Platform;
import org.uu.nl.net2apl.core.platform.PlatformNotFoundException;

import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;

public class ReceiveDBSuggestionsPlan extends MessagePlan {

    public ReceiveDBSuggestionsPlan(ACLMessage message, GAMessageHeader header, FIPASendableObject content) {
        super(message, header, content);
    }

    /**
     * This method is executed when a message has been received
     *
     * @param planInterface   An interface to the agent in order to access context, etc
     * @param receivedMessage The message that triggered this plan
     * @param header          The header of the message
     * @param content         The content of the message
     * @throws PlanExecutionError Exception thrown when executing this plan goes totally awry
     */
    @Override
    public void executeOnce(PlanToAgentInterface planInterface, ACLMessage receivedMessage, GAMessageHeader header, FIPASendableObject content) throws PlanExecutionError {
        AqlDbTypeSuggestionWrapper suggestions;
        try {
            suggestions = (AqlDbTypeSuggestionWrapper) ((GAMessageContentWrapper) message.getContentObject()).getContent();
        } catch (UnreadableException e) {
            logger.log(getClass(), "Unable to parse AQL DB Type Suggestion Wrapper object. Plan failed");
            logger.log(getClass(), e);
            throw new PlanExecutionError();
        }

        BrokerSearchSuggestionsContext suggestionsContext = planInterface.getContext(BrokerSearchSuggestionsContext.class);

        SearchSuggestion suggestion = suggestionsContext
                .getSubscription(message.getConversationId())
                .getSearchSuggestions(suggestions.getTargetAqlQueryId());

        suggestion.getModel().setAgentSuggestionsReceived(message.getSender(), true);

        BrokerSearchSuggestionsContext.ContactedAgent contactedAgent =
                suggestion.getContactedAgent(receivedMessage.getSender());

        if (contactedAgent.getState().equals(BrokerSearchSuggestionsContext.STATUS.INITIAL_FINALIZING)) {
            contactedAgent.setState(BrokerSearchSuggestionsContext.STATUS.INITIAL_DONE);
        } else if (!contactedAgent.isInitial() && contactedAgent.getState().equals(BrokerSearchSuggestionsContext.STATUS.EXTENDED_STARTED)) {
            contactedAgent.setState(BrokerSearchSuggestionsContext.STATUS.DONE);
        }

        suggestionsContext.addDbSuggestionsForQuery(
                message.getConversationId(),
                suggestion.getQuery(),
                receivedMessage.getSender(),
                suggestions
        );
        boolean noMoreSuggestionsExpected = contactOtherAgents(
                planInterface,
                suggestion,
                suggestions
        );

        try {
            DbBasedSuggestSearchOptionsPlan plan = new DbBasedSuggestSearchOptionsPlan(
                    suggestion.getReceivedMessage(),
                    suggestion.getUserQueryTriggerMessageHeader(),
                    suggestion.getReceivedMessage().getContentObject()
            );
            if (noMoreSuggestionsExpected) {
                plan.setNoMoreSuggestionsExpected(true);
            }
            planInterface.adoptPlan(plan);
        } catch (UnreadableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tries to find if more agents need to be contacted for suggestion request.
     * If so, does contact them
     * @param planInterface
     * @param suggestion
     * @param suggestions
     * @return True iff no more suggestions are expected, i.e., no more agents are contacted and thus the suggestions
     * are finished
     */
    private boolean contactOtherAgents(
            PlanToAgentInterface planInterface,
            SearchSuggestion suggestion,
            AqlDbTypeSuggestionWrapper suggestions
    ) {
        if (suggestion.initialAgentsDone()) {
            BrokerContext context = planInterface.getContext(BrokerContext.class);
            Set<AgentID> uncontactedAgents = SetUtils.difference(
                    context.getDbAgentExpertises().keySet(),
                    suggestion.getContactedAgents()
            );
            if (uncontactedAgents.size() > 0) {
                for(AgentID agentID : uncontactedAgents) {
                    // Add as non-initial agent
                    Platform.getLogger().log(getClass(), Level.SEVERE, String.format(
                            "Contacted agent %s who was not an original contributor to the query result",
                            agentID
                    ));
                    BrokerSearchSuggestionsContext.ContactedAgent contactedAgent = suggestion.getContactedAgent(agentID, false);
                    Set<EntityList.Entity> toRequest = suggestion.getSerializableFocusEntities().getEntitySet();
                    Set<EntityList.Entity> actuallyMapped = SetUtils.intersection(
                            suggestion.getMappedEntities(),
                            toRequest
                    );
                    contactedAgent.addEntities(actuallyMapped);
                    contactOtherAgent(
                            planInterface,
                            suggestions.getTargetAqlQueryId(),
                            agentID,
                            actuallyMapped
                            );
                }
            } else if (suggestion.allAgentsDone()) {
                return true;
            } else {
                for(AgentID agentID : suggestion.getContactedAgents()) {
                    BrokerSearchSuggestionsContext.ContactedAgent agent = suggestion.getContactedAgent(agentID);
                    Platform.getLogger().log(getClass(), Level.SEVERE, String.format(
                            "Contacting %s, who was an original contributor, with entities not yet requested of them",
                            agentID
                    ));
                    if (agent.isInitial()) {
                        // TODO, we are assuming SameAs entities are already resolved in the model. Maybe should be verified
                        Set<EntityList.Entity> notYetSent = SetUtils.difference(
                                suggestion.getSerializableFocusEntities().getEntitySet(),
                                agent.getEntities()
                        );
                        Set<EntityList.Entity> actuallyMapped = SetUtils.intersection(
                                suggestion.getMappedEntities(),
                                notYetSent
                        );
                        if (notYetSent.size() == 0) {
                            Platform.getLogger().log(getClass(), Level.SEVERE, String.format(
                                    "Agent %s has no more unsent entities. Marking as done",
                                    agentID
                            ));
                            agent.setState(BrokerSearchSuggestionsContext.STATUS.DONE);
                            return true;
                        } else {
                            agent.addEntities(actuallyMapped);
                        }
                        contactOtherAgent(
                                planInterface,
                                suggestions.getTargetAqlQueryId(),
                                agentID,
                                actuallyMapped
                        );
                    }
                }
            }
        }
        return false;
    }

    private void contactOtherAgent(
            PlanToAgentInterface planToAgentInterface,
            int targetAqlQueryID,
            AgentID agentID,
            Set<EntityList.Entity> entities
    ) {
        EntityList entityList = new EntityList(targetAqlQueryID, entities);
        entityList.setFromSource(false);

        ACLMessage m = this.message.createForward(planToAgentInterface.getAgentID(), agentID);
        m.setPerformative(Performative.REQUEST);
        try {

            m.setContentObject(
                    new GAMessageContentWrapper(
                            GAMessageHeader.REQUEST_SUGGESTIONS,
                            entityList
                    )
            );
        } catch (IOException e) {
            logger.log(getClass(), e);
        }

        try {
            planToAgentInterface.getAgent().sendMessage(m);
            logger.log(getClass(), Level.INFO, String.format(
                    "Requested suggestions for %d entities from db agent %s",
                    entities.size(), agentID));
        } catch (PlatformNotFoundException | MessageReceiverNotFoundException e) {
            logger.log(getClass(), e);
        }
    }
}
