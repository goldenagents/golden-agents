package org.uu.nl.goldenagents.netmodels.angular.aql;

import org.uu.nl.goldenagents.aql.AQLQuery;
import org.uu.nl.goldenagents.aql.AQLTree;
import org.uu.nl.goldenagents.aql.MostGeneralQuery;
import org.uu.nl.goldenagents.aql.complex.*;
import org.uu.nl.goldenagents.aql.feature.NamedLiteral;
import org.uu.nl.goldenagents.aql.feature.NamedResource;
import org.uu.nl.goldenagents.aql.feature.TypeSpecification;
import org.uu.nl.net2apl.core.platform.Platform;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class NewAQLJsonBuilder {

    private final AQLQuery query;
    private final List<AQLQueryJsonRow> rows;
    private boolean isBuilding;
    private boolean inFocus = false;
    private UUID conversationID;
    private boolean canUnion;

    public NewAQLJsonBuilder(AQLQuery query, UUID conversationID) {
        this.conversationID = conversationID;
        this.query = query;
        this.rows = new ArrayList<>();
        this.isBuilding = true;

        // Determine how to start the query
        StringBuilder startFiller = new StringBuilder();
        startFiller.append("Give me ");
        if (this.query.getQueryTree() instanceof BinaryAQLInfixOperator) {
            BinaryAQLInfixOperator queryTree = (BinaryAQLInfixOperator) this.query.getQueryTree();
            if (hasTypeAtCurrentFocus(queryTree)) {
                startFiller.append("every");
            } else if (queryTree.getRightChild() instanceof CrossingOperator) {
                startFiller.append("everything");
            }
        }

        AQLQueryJsonRow row = new AQLQueryJsonRow(0);
        this.rows.add(row);
        addFillerToRow(row, new MostGeneralQuery(), startFiller.toString());
        decompose(this.query.getQueryTree(), row,1, true, false, false);
    }

    private boolean firstTypeAdded = false;

    private boolean decompose(AQLTree tree, AQLQueryJsonRow activeRow, int indentation, boolean firstIntersection, boolean isChildOfFocus, boolean encounteredCrossing) {
        boolean isInFocus = isChildOfFocus || this.query.getFocusName().equals(tree.getFocusName());
        if (tree instanceof MostGeneralQuery) {
            mostGeneralToJson(activeRow, (MostGeneralQuery) tree, isInFocus);
        } else if (tree instanceof Union) {
            Union union = (Union) tree;
            AQLTree leftChild = union.getLeftChild();
            AQLTree rightChild = union.getRightChild();

            activeRow = new AQLQueryJsonRow(indentation);
            this.rows.add(activeRow);
            if (leftChild instanceof MostGeneralQuery) {
                addFillerToRow(activeRow, union, "that is");
                mostGeneralToJson(activeRow, (MostGeneralQuery) leftChild, isChildOfFocus);
            } else {
                decompose(leftChild, activeRow, indentation, true, isInFocus, false);
            }
            activeRow = new AQLQueryJsonRow(indentation );
            rows.add(activeRow);
            addFillerToRow(activeRow, union, "or");
            activeRow = new AQLQueryJsonRow(indentation);
            rows.add(activeRow);
            if (rightChild instanceof MostGeneralQuery) {
                addFillerToRow(activeRow, union, "that is");
                mostGeneralToJson(activeRow, (MostGeneralQuery) rightChild, isChildOfFocus);
            } else {
                decompose(rightChild, activeRow, indentation, true, isChildOfFocus, false);
            }
        } else if (tree instanceof Intersection) {
            Intersection intersection = (Intersection) tree;
            AQLTree leftChild = intersection.getLeftChild();
            AQLTree rightChild = intersection.getRightChild();

            if (rightChild instanceof MostGeneralQuery && !(leftChild instanceof MostGeneralQuery)) {
                decompose(leftChild, activeRow, indentation, firstIntersection, isInFocus, encounteredCrossing);
            } else if (leftChild instanceof MostGeneralQuery && !(rightChild instanceof MostGeneralQuery)) {
                decompose(rightChild, activeRow, indentation, firstIntersection, isInFocus, encounteredCrossing);
            } else if (!(leftChild instanceof MostGeneralQuery)) {
                boolean hasCrossing = hasCrossing(leftChild) && !(leftChild instanceof Intersection);
                if (encounteredCrossing || hasCrossing) {
                    activeRow = new AQLQueryJsonRow(indentation);
                    this.rows.add(activeRow);
                    if (encounteredCrossing) {
                        addFillerToRow(activeRow, leftChild, "and");
                    }
                }
                encounteredCrossing |= decompose(leftChild, activeRow, indentation, false, isInFocus, encounteredCrossing);

                if (encounteredCrossing) {
                    activeRow = new AQLQueryJsonRow(indentation);
                    this.rows.add(activeRow);
                    addFillerToRow(activeRow, leftChild, "and");
                }
                decompose(rightChild, activeRow, indentation, false, isInFocus, encounteredCrossing);
                if (activeRow.elements.size() == 1 && activeRow.elements.get(0).getType().equals("filler")) {
                    this.rows.remove(activeRow);
                    Platform.getLogger().log(getClass(), Level.SEVERE, "Why do we have a row with ONLY a filler!?");
                }
            }
        } else if (tree instanceof TypeSpecification) {
            activeRow.addElement(new AQLQueryJsonObject(
                    tree.getFocusName(),
                    "class",
                    tree.getAQLLabel(),
                    firstTypeAdded ? "that is a" : "",
                    isInFocus && this.query.variableIsFocus(tree) && !(this.query.getFocus() instanceof CrossingOperator),
                    isInFocus,
                    tree.getFocusName()
            ));
            this.canUnion = this.query.getFocusName().equals(tree.getFocusName());
            firstTypeAdded = true;
        } else if (tree instanceof CrossingOperator) {
            CrossingOperator crossingOperator = (CrossingOperator) tree;
            String type = tree instanceof CrossingBackwards ? "backward" : "forward";
            String prefix = tree instanceof CrossingBackwards ? "that has a" : "that is the";

            MostGeneralQuery mostGeneralSubQuery = query.getMostGeneralQueryWithSameFocusVar(crossingOperator.getSubquery());
            boolean getsFocusOfChild = query.getFocusName().equals(mostGeneralSubQuery.getFocusName()); //crossingOperator.getSubquery() instanceof MostGeneralQuery && this.query.getFocusName().equals(crossingOperator.getSubquery().getFocusName());
            isInFocus |= getsFocusOfChild;

            activeRow.addElement(new AQLQueryJsonObject(
                    crossingOperator.getFocusName(),
                    type,
                    crossingOperator.getAQLLabel(),
                    prefix,
                    crossingOperator.getFocusName().equals(this.query.getFocusName()) || getsFocusOfChild,
                    isInFocus,
                    mostGeneralSubQuery.getFocusName()
            ));
//            this.canUnion |= ((query.getFocusName().equals(tree.getFocusName()))); // TODO
            if (!(crossingOperator.getSubquery() instanceof MostGeneralQuery)) {
                decompose(crossingOperator.getSubquery(), activeRow, indentation+1, true, isInFocus, false);
            }
            return true;
        } else if (tree instanceof NamedResource || tree instanceof NamedLiteral) {
            activeRow.addElement(new AQLQueryJsonObject(
                    tree.getFocusName(),
                    "instance",
                    tree.getAQLLabel(),
                    "that is",
                    tree.getFocusName().equals(this.query.getFocusName()),
                    isInFocus,
                    tree.getFocusName()
            ));
            this.canUnion |= query.getFocusName().equals(tree.getFocusName());
        }
        return false;
    }

    public boolean hasCrossing(AQLTree tree) {
        if (tree instanceof CrossingOperator) return true;
        for (AQLTree subquery : tree.getSubqueries()) {
            boolean hasCrossing = hasCrossing(subquery);
            if (hasCrossing) return true;
        }
        return false;
    }

    private AQLTree getTopLevelNodeInSameFocus(AQLTree tree) {
        if (tree.getParentID() == null) return tree;
        AQLTree parent = this.query.getFoci().get(tree.getParentID());
        if (parent instanceof CrossingOperator) return tree;
        else return getTopLevelNodeInSameFocus(parent);
    }

    private MostGeneralQuery getMostGeneralQueryInSameFocus(AQLTree tree) {
        return getFirstMostGeneralQueryInTree(getTopLevelNodeInSameFocus(tree));
    }

    private MostGeneralQuery getFirstMostGeneralQueryInTree(AQLTree tree) {
        if (tree instanceof MostGeneralQuery) return (MostGeneralQuery) tree;
        for(AQLTree subtree : tree.getSubqueries()) {
            if (!(subtree instanceof CrossingOperator)) {
                MostGeneralQuery q = getFirstMostGeneralQueryInTree(subtree);
                if (q != null) return q;
            }
        }
        return null;
    }

    private void mostGeneralToJson(AQLQueryJsonRow row, MostGeneralQuery node, boolean isChildOfFocus) {
        boolean isChildOfUnion = node.getParentID() != null && this.query.getNode(node.getParentID()) instanceof Union;
        if((isChildOfFocus && this.query.getQueryTree().getSubqueries().isEmpty()) || isChildOfUnion) {
            row.addElement(new AQLQueryJsonObject(
                    node.getFocusName(),
                    "class",
                    isChildOfUnion ? "anything" : "everything",
                    "",
                    node.getFocusName().equals(this.query.getFocusName()),
                    isChildOfFocus,
                    node.getFocusName()
            ));
        }
    }

    public void addFillerToRow(AQLQueryJsonRow row, AQLTree tree, String label) {
        AQLTree.ID focusTarget = tree.getFocusName();
        row.addElement(new AQLQueryJsonObject(
                tree.getFocusName(),
                "filler",
                label,
                "",
                false,
                inFocus,
                focusTarget
        ));
    }

    /**
     * Checks if at the current focus variable, there is a TypeSpecification object, indicating that the query should
     * start with "every"
     *
     * @param tree Tree to check
     * @return True iff the root focus variable includes a type specification
     */
    private boolean hasTypeAtCurrentFocus(AQLTree tree) {
        if (tree instanceof TypeSpecification) {
            return true;
        } else if (tree instanceof CrossingOperator) {
            return false;
        } else {
            for(AQLTree child : tree.getSubqueries()) {
                if (hasTypeAtCurrentFocus(child)) return true;
            }
            return false;
        }
    }

    public AQLJsonObject build() {
        boolean canUnion = !(this.query.getFocus() instanceof MostGeneralQuery);
        return new AQLJsonObject(this.rows, this.conversationID, this.query.getFocusName(), this.query.getFocusName(), Integer.toString(this.query.hashCode()), canUnion);
    }
}
