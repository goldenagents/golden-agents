package org.uu.nl.goldenagents.agent.plan.dbagent;

import org.apache.jena.graph.impl.LiteralLabel;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.uu.nl.goldenagents.agent.context.DBAgentContext;
import org.uu.nl.goldenagents.agent.context.query.DbTranslationContext;
import org.uu.nl.goldenagents.agent.plan.MessagePlan;
import org.uu.nl.goldenagents.netmodels.AqlDbTypeSuggestionWrapper;
import org.uu.nl.goldenagents.netmodels.fipa.EntityList;
import org.uu.nl.goldenagents.netmodels.fipa.GAMessageContentWrapper;
import org.uu.nl.goldenagents.netmodels.fipa.GAMessageHeader;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.defaults.messenger.MessageReceiverNotFoundException;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.FIPASendableObject;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.fipa.acl.UnreadableException;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;
import org.uu.nl.net2apl.core.platform.Platform;
import org.uu.nl.net2apl.core.platform.PlatformNotFoundException;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

/**
 * This plan uses SPARKLIS-type queries to find suggestions for an entity
 */
public class SparklisTypeSuggestionsPlan extends MessagePlan {

    private PlanToAgentInterface planInterface;

    public SparklisTypeSuggestionsPlan(ACLMessage message, GAMessageHeader header, FIPASendableObject content) {
        super(message, header, content);
    }

    /**
     * This method is executed when a message has been received
     *
     * @param planInterface   An interface to the agent in order to access context, etc
     * @param receivedMessage The message that triggered this plan
     * @param header          The header of the message
     * @param content         The content of the message
     * @throws PlanExecutionError Exception thrown when executing this plan goes totally awry
     */
    @Override
    public void executeOnce(PlanToAgentInterface planInterface, ACLMessage receivedMessage, GAMessageHeader header, FIPASendableObject content) throws PlanExecutionError {
        this.planInterface = planInterface;
        EntityList entityList = EntityList.fromACLMessage(receivedMessage);
        DBAgentContext context = planInterface.getContext(DBAgentContext.class);
        DbTranslationContext translator = planInterface.getContext(DbTranslationContext.class);
        List<EntityList.Entity> entitiesValues = this.getEntities(entityList.getTargetAQLQueryID(), entityList.isFromSource() ? 1000 : entityList.getEntitySet().size()); // TODO what value can / should we use here?

        Map<String, String> customPrefixMap = new HashMap<>();
        List<String> filterEntities = new ArrayList<>();

        for(EntityList.Entity entity : entitiesValues) {
            if (entity.isUri()) {
                // TODO, split code below for cases above
                String uri = entity.getAsNode().getURI();
                int splitPosition = Math.max(uri.lastIndexOf('/'), uri.lastIndexOf('#')) + 1;
                String namespace = uri.substring(0, splitPosition);
                String prefix;
                if (customPrefixMap.containsKey(namespace)) {
                    prefix = customPrefixMap.get(namespace);
                } else {
                    prefix = String.format("ns%d", customPrefixMap.size());
                    customPrefixMap.put(namespace, prefix);
                }
                String localName = uri.substring(splitPosition);
                filterEntities.add(String.format("%s:%s", prefix, localName));
            } else {
                LiteralLabel lbl = entity.getAsNode().getLiteral();
                String lexicalForm = entity.getAsNode().toString();
                if (lexicalForm.contains("die ohnehin mehr eingebildeten als reellen")) {
                    System.out.println("breakpoint");
                }
                String value = lbl.getLexicalForm();
                String uri = lbl.getDatatypeURI();
                String localName = null;
                String prefix = null;
                if (uri != null) {
                    int splitPosition = Math.max(uri.lastIndexOf('/'), uri.lastIndexOf('#')) + 1;
                    String namespace = uri.substring(0, splitPosition);
                    if (customPrefixMap.containsKey(namespace)) {
                        prefix = customPrefixMap.get(namespace);
                    } else {
                        prefix = String.format("ns%d", customPrefixMap.size());
                        customPrefixMap.put(namespace, prefix);
                    }
                    localName = uri.substring(splitPosition);
                }
                if (prefix != null) {
                    filterEntities.add(String.format("\"%s\"^^%s:%s", value, prefix, localName));
                } else {
                    filterEntities.add(lexicalForm);
                }
            }
        }

        Set<Resource> classes = new HashSet<>();
        Set<Resource> forward = new HashSet<>();
        Set<Resource> backward = new HashSet<>();

        int startIndex = 0;
        while (startIndex < filterEntities.size()) {
            StringBuilder query = new StringBuilder();
            for(String ns : customPrefixMap.keySet()) {
                query.append(String.format("PREFIX %s: <%s>", customPrefixMap.get(ns), ns));
            }
            query.append("\nSELECT distinct ?class ?forward ?backward WHERE {\n" +
                            "{\n" +
                            "{ ?x a ?class} UNION {?x ?backward []} UNION {[] ?forward ?x}\n" +
                            "}");

            int to = Math.min(filterEntities.size(), startIndex+4094);
            query.append(String.format(" . FILTER(?x IN (%s))", String.join(",", filterEntities.subList(startIndex, to))));
            startIndex += 4094;

            query.append("\n}");
            Platform.getLogger().log(getClass(), Level.INFO, query.toString());
            Map<String, Set<Resource>> resourceMap = executeQuery(context, translator, query.toString());

            int c = classes.size();
            int f = forward.size();
            int b = backward.size();

            classes.addAll(resourceMap.get("classes"));
            forward.addAll(resourceMap.get("forward"));
            backward.addAll(resourceMap.get("backward"));

            int added = (classes.size() - c) + (forward.size() - f) + (backward.size() - b);

            if (startIndex >= filterEntities.size() || added > 0) {
                // Construct reply message by finding suggestions
                AqlDbTypeSuggestionWrapper messageWrapper = new AqlDbTypeSuggestionWrapper(
                        classes,
                        forward,
                        backward,
                        entityList.getTargetAQLQueryID(),
                        UUID.fromString(message.getConversationId())
                );

                ACLMessage response = message.createReply(planInterface.getAgentID(), Performative.INFORM_REF);
                try {
                    response.setContentObject(new GAMessageContentWrapper(GAMessageHeader.INFORM_SUGGESTIONS, messageWrapper));
                    logger.log(getClass(), "Sending query suggestions for query to broker");
                    planInterface.getAgent().sendMessage(response);
                } catch (IOException | MessageReceiverNotFoundException | PlatformNotFoundException e) {
                    logger.log(getClass(), e);
                }
            }
        }


    }

    /**
     * Parses broker message for entity list
     *
     * @return List of entities, reduced to maxSize
     */
    private List<EntityList.Entity> getEntities(int targetAQLQueryID, int sampleSize) {
        EntityList entityList = new EntityList(targetAQLQueryID);
        try {
            entityList = (EntityList) ((GAMessageContentWrapper) message.getContentObject()).getContent();
        } catch (UnreadableException e) {
            logger.log(getClass(), e);
        }

//        return entityList.getEntities();
        return entityList.getSublist(sampleSize);
    }

    /**
     * Performs a query to find query suggestions on the database of this agent, and collects the results in a list
     *
     * @param context DBAgentContext of the current DB agent
     * @param query   A SelectBuilder query that requests the desired properties
     * @return Map containing (with equivalent keys) the classes, forward and backward crossing properties associated with the query
     */
    private Map<String, Set<Resource>> executeQuery(
            DBAgentContext context,
            DbTranslationContext translator,
            String query) {

        Set<Resource> classes = new HashSet<>();
        Set<Resource> forward = new HashSet<>();
        Set<Resource> backward = new HashSet<>();
        Set<String> capabilities = new HashSet<>(context.getExpertise().getCapabilities());

        try (DBAgentContext.DbQuery q = context.getDbQuery(query)) {
            ResultSet results = q.queryExecution.execSelect();
            while (results.hasNext()) {
                QuerySolution solution = results.next();
                if (solution.contains("?class")) {
                    Resource clazz = solution.getResource("?class");
                    List<DbTranslationContext.Translation> translations = translator.getLocalToGlobalTranslation(clazz);
                    if (translations == null) {
                        String shortForm = translator.shortForm(clazz.getURI());
                        if (capabilities.contains(clazz.getURI()) || capabilities.contains(shortForm)) {
                            classes.add(clazz);
                        }
                    } else {
                        for (DbTranslationContext.Translation translation : translations) {
                            classes.add(translation.getGlobalConcept());
                        }
                    }
                } else {
                    Resource property;
                    boolean isForward = true;
                    if (solution.contains("?forward")) {
                       property = solution.getResource("?forward");
                    } else {
                        property = solution.getResource("?backward");
                        isForward = false;
                    }
                    List<DbTranslationContext.Translation> translations = translator.getLocalToGlobalTranslation(property);
                    if (translations == null) {
                        String shortForm = translator.shortForm(property.getURI());
                        if (capabilities.contains(property.getURI()) || capabilities.contains(shortForm)) {
                            if (isForward) {
                                forward.add(property);
                            } else {
                                backward.add(property);
                            }
                        }
                    } else {
                        for (DbTranslationContext.Translation translation : translations) {
                            if(isForward ^ translation.isInverse()) {
                                forward.add(translation.getGlobalConcept());
                            } else {
                                backward.add(translation.getGlobalConcept());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            String dataUri = this.planInterface.getContext(DBAgentContext.class).getRdfDataURI();
            String reason = e.getMessage() == null ? e.toString() : e.getMessage();
            Platform.getLogger().log(getClass(), Level.SEVERE, String.format("Error executing DB Suggestion query on %s! Because %s", dataUri, reason));
            Platform.getLogger().log(getClass(), Level.SEVERE, query.toString());
        }

        // Returning more than one object? Not to worry, just take the Python approach!!! #yolo
        Map<String, Set<Resource>> suggestions = new HashMap<>();
        suggestions.put("classes", classes);
        suggestions.put("forward", forward);
        suggestions.put("backward", backward);
        return suggestions;
    }
}
