# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

## Docker
A docker image can be built and run, using

```bash
docker build -t golden-agents/frontend .
docker run -dp <port>:<internal-docker-port> [--env API_URL=<api-url>] [--env API_PORT=<api-port>] [--env LISTEN_PORT=<internal-docker-port>] golden-agents/frontend
```

Where:
* `<internal-docker-port>` is the port *within* the Docker container to which nginx listens. Defaults to `80`
* `<port>` is the port on which to expose the system, e.g. 80
* `<api-url>` is the url from which the backend is accessible. If the golden-agents/backend container runs on the
same host as this container, `http://localhost` or `http://127.0.0.1` can be used, although it is better to provide a resolvable IP 
  address or a fully qualified domain name.
* `<api-port>` is the port on the host provided at `<api-url>` from which the backend is accessible.

From the system on which the docker image was run, the system can be accessed from a web browser at 
localhost:<port> (or just localhost if `<port>=80`)

WARNING: Make sure to add the full qualified domain name at which this container is accessible to the ALLOWED_ORIGINS environment
parameter in the golden-agents/backend container to work with the CORS policy.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
