import { CrudAgent } from '../models/crudagent';
import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'agentTypeFilter',
  pure: false,
})
@Injectable()
export class AgentTypeFilterPipe implements PipeTransform {

  transform(agents: CrudAgent[], filter: string): CrudAgent[] {
    return agents.filter(agent => agent.agentType === filter);
  }

}
