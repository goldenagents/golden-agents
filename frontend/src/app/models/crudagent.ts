import { DummyAgent } from './dummyagent';

export class CrudAgent {

  public constructor(
    public agentType: string,
    public icon: string,
    public nickname: string,
    public host: string,
    public port: number,
    public uuid: string,
    public additionals: { [name: string]: string },
    public ready = false,
    public description: string = null,
    public homepage: string = null
  ) {}


  public static fromEventData(data: any) {
    return new CrudAgent (
      data.agentType,
      data.icon,
      data.nickname,
      data.host,
      data.port,
      data.uuid,
      data.additionals,
      data.ready,
      data.description,
      data.homepage
    );
  }

  public static fromDummy(dummy: DummyAgent): CrudAgent {
    return new CrudAgent (
      dummy.agentType,
      '',
      dummy.nickname,
      '',
      -1,
      '',
      dummy.additionals,
    );
  }
}
