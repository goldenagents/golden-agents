export class DummyMessage {
  public receiverUUID: string;
  public receiverHost: string;
  public receiverPort: number;
  public receiverNickname: string;
  public performative: string;
  public content: string;
  public constructor(
    public senderUUID: string,
    public senderHost: string,
    public senderPort: number,
    public senderNickname: string
  ) {}
}
