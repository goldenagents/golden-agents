export class DummyAgent {
  public constructor(
    public agentType: string,
    public nickname: string,
    public additionals: { [name: string]: string }) {}
}
