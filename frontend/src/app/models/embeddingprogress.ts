

export class EmbeddingProgress {
    public constructor(
    public index: number,
    public type: string,
    public value: number,
    public finished: boolean) {}

  public static fromEventData(data: any) {
      return new EmbeddingProgress(
      +data.index,
      data.type,
      +data.value,
      data.finished
    );
  }
}
