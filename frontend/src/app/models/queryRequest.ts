export class QueryRequest {
  public queryID: string;
  public constructor(
    public query: string,
    public queryType: string,
    public selectedSources: string[]
  ) {}
}
