export class QueryParseException {

  public isPopulated = false;

  public constructor(
    public originalErrorMessage: string,
    public humanErrorMessage: string,
    public encounteredString: string,
    public expectedString: string[],
    public line: number,
    public column: number,
    public nCharacters: number
  ) {
    this.isPopulated = true;
  }

  public static fromEventData(data: any) {
    return new QueryParseException(
      data.error,
      data.humanMessage,
      data.encountered,
      data.expected,
      data.line,
      data.column,
      data.nCharacters
    );
  }
}
