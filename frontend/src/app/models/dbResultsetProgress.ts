export class DbResultsetProgress {
  public finished: boolean;
  public error: boolean;
  public errorMessage: string;
  public results: number;
  public class: string;

  public constructor(
    public name: string
  ) {
    this.finished = false;
    this.results = 0;
    this.class = 'in-progress';
  }

  public setFinished(finished: boolean): void {
    this.finished = finished;
    this.class = finished ? 'progress-done' : 'in-progress';
  }

  public setError(message: string): void {
    this.error = true;
    this.errorMessage = message;
    this.class = 'progress-error';
  }
}

export interface IAgentHash {
  [agentname: string]: DbResultsetProgress;
}
