import {AQLQueryRow} from './AQLQueryRow';

export class AQLQuery {
  public constructor(
    public rows: AQLQueryRow[],
    public conversationID: string,
    public virtualFocus: string,
    public focus: string,
    public queryID: string,
    public canUnion: boolean
  ){ }

  public static fromData(data: any): AQLQuery {
    return new AQLQuery(
      data.rows.map(row => AQLQueryRow.fromData(row)),
      data.conversationID,
      data.virtualFocus,
      data.focus,
      data.queryID,
      data.canUnion
    );
  }
}
