import {AQLQueryBlock} from './AQLQueryBlock';

export class AQLQueryRow {
  constructor(
    public indentation: number,
    public elements: AQLQueryBlock[]
  ) { }

  public static fromData(data: any): AQLQueryRow {
    return new AQLQueryRow(
      data.indentation,
      data.elements.map(x => AQLQueryBlock.fromData(x))
    );
  }

}
