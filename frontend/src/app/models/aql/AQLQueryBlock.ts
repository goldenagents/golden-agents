export class AQLQueryBlock {
  constructor(
    public name: string,
    public type: string,
    public label: string,
    public prefix: string,
    public focus: boolean,
    public inFocus: boolean,
    public focusTarget: string,
  ) { }

  public static fromData(data: any): AQLQueryBlock {
    return new AQLQueryBlock(
      data.name,
      data.type,
      data.label,
      data.prefix,
      data.focus,
      data.inFocus,
      data.focusTarget
    );
  }
}
