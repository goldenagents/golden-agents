import {AQLTypeItem} from './AQLTypeItem';
import {AQLIdentityItem} from './AQLIdentityItem';

export class AQLSuggestions {

  constructor(
    public classes: AQLTypeItem[],
    properties: AQLTypeItem[],
    public instances: AQLIdentityItem[],
    public queryID: number,
    public focus: string,
    public finished: boolean,
    public selected?: boolean
  ) {
    this.forwardProperties = properties.filter(e => e.forwardjump);
    this.backwardProperties = properties.filter(e => !e.forwardjump);
  }

  public forwardProperties: AQLTypeItem[];
  public backwardProperties: AQLTypeItem[];

  static fromEventData(eventData): AQLSuggestions {
    if (eventData == null) { return null; }
    console.log({eventData});
    return new AQLSuggestions(
      eventData.classList.map(c => AQLTypeItem.fromData(c)),
      eventData.propertyList.map(p => AQLTypeItem.fromData(p)),
      eventData.instanceList.map(i => AQLIdentityItem.fromData(i)),
      eventData.targetAqlQueryId,
      eventData.focusID,
      eventData.finished,
      false
    );
  }
}
