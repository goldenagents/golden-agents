export class AQLTypeItem {
  constructor(
    public label: string,
    public type: string,
    public subitems: AQLTypeItem[],
    public url: string,
    public quantifier: string,
    public forwardjump: boolean // Forwards means it is the p of something (y p x, with y is fresh)
  ) {}

  public static fromData(d): AQLTypeItem {
    return new AQLTypeItem(
      d.label,
      d.type,
      d.subitems.map(s => AQLTypeItem.fromData(s)),
      d.url,
      d.quantifier,
      d.forwardjump
    );
  }
}
