export class AQLIdentityItem {
  constructor(
    public type: string,
    public dataTypeURI: string,
    public label: string,
    public target: string,
    public title: string,
  ) { }

  static fromData(d): AQLIdentityItem {
    return new AQLIdentityItem(d.type, d.dataTypeURI, d.label, d.target, d.title);
  }
}
