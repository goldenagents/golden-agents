export class QueryProgressSubResult {
  public constructor(
    public stringvalue: string,
    public longvalue: number,
    public finished: boolean
  ) { }

  public static fromEventData(data: any) {
    return new QueryProgressSubResult(data.stringValue, data.longValue, data.finished);
  }
}
