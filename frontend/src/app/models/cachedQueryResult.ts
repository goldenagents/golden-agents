export class CachedQueryResult {
    public constructor(
      public headers: string,
      public queryID: string,
      public conversationID: string,
      public query: string,
      public addedAt: Date,
      public resultsFinishedAt: Date,
      public finished: boolean,
      public selectedSources: string[]
    ) {}

    public static fromData(data: any): CachedQueryResult {
      return new CachedQueryResult(
        data.headers,
        data.queryID,
        data.conversationID,
        data.queryString,
        new Date(data.added),
        new Date(data.resultsCollected),
        data.finished,
        data.selectedSources
      );
  }
}
