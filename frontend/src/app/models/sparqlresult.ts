export class SparqlResult {

  public constructor(
    public firstIndex: number,
    public lastIndex: number,
    public results: string,
    public conversationId: string,
    public uuid: string) {}

  public static fromEventData(data: any) {
    return new SparqlResult (
      data.firstIndex,
      data.lastIndex,
      data.results,
      data.conversationId,
      data.uuid
    );
  }
}
