import { DummyMessage } from './dummymessage';

export class CrudMessage {

  constructor(
    public messageID: string,
    public time: string,
    public received: boolean,
    public conversationID: string,
    public senderUUID: string,
    public senderNickname: string,
    public senderHost: string,
    public senderPort: number,
    public receiverUUID: string,
    public receiverNickname: string,
    public receiverHost: string,
    public receiverPort: number,
    public performative: string,
    public content: string,
    public encoding: string,
    public language: string,
    public ontology: string,
    public header: string,
    public params: Map<string, string>
  ) {}

  public static fromEventData(data: any) {
    return new CrudMessage(
      data.messageID,
      data.time,
      data.received,
      data.conversationID,
      data.senderUUID,
      data.senderNickname,
      data.senderHost,
      data.senderPort,
      data.receiverUUID,
      data.receiverNickname,
      data.receiverHost,
      data.receiverPort,
      data.performative,
      data.content,
      data.encoding,
      data.language,
      data.ontology,
      data.header,
      data.params
    );
  }

  public static fromDummy(dummy: DummyMessage): CrudMessage {
    return new CrudMessage (
      '', // messageID
      '', // time
      false, // received
      '', // conversationID
      dummy.senderUUID,
      dummy.senderNickname,
      dummy.senderHost,
      dummy.senderPort,
      dummy.receiverUUID,
      dummy.receiverNickname,
      dummy.receiverHost,
      dummy.receiverPort,
      dummy.performative,
      dummy.content,
      '', // encoding
      '', // language
      '', // encoding
      '', // header
      null // params
    );
  }
}
