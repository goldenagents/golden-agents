import {QueryProgressSubResult} from './queryProgressSubResult';

export class QueryProgress {

  public constructor(
    public conversationId: string,
    public index: number,
    public type: string,
    public value: any,
    public finished: boolean,
    public subresults: QueryProgressSubResult[]) {}

  public static fromEventData(data: any) {
      const subresults: any[] = data.subresults;
      const sr: QueryProgressSubResult[] = [];
      if (subresults !== null) {
        subresults.forEach((value: any, index: number) => {
          sr.push(QueryProgressSubResult.fromEventData(value));
        });
      }

      return new QueryProgress(
      data.conversationId,
      data.index,
      data.type,
      data.value,
      data.finished,
      sr
    );
  }
}
