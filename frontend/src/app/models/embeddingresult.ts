export class EmbeddingResult {

  public constructor(
    public vector: number[],
    public uri: string,
    public distance: string
  ) {}

  public static fromData(data: any) {
    return new EmbeddingResult(
      data['vector'],
      data['uri'],
      data['distance']
    );
  }
}
