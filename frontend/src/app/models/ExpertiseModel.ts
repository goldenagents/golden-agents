export class ExpertiseModel {
  label: string;
  count: number;
  class: boolean;
  index: number;  // Populated by frontend
  color: string;  // Populated by frontend
  combinations?:
    {
      label: string;
      count: number;
      class: boolean;
      index: number;  // Populated by frontend
      color: string;  // Populated by frontend
    }[];

  static fromCombination(json: ExpertiseModel, labelOnly: boolean): ExpertiseModel {
    const model = new ExpertiseModel();
    model.label = json.label;
    model.class = json.class;
    if (!labelOnly) {
      model.count = json.count;
    } else {
      model.count = 0;
    }
    return model;
  }
}
