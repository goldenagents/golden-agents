import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {UseragentService} from './useragent.service';
import {CrudAgent} from '../models/crudagent';
import {AQLQuery} from '../models/aql/AQLQuery';

@Injectable({
  providedIn: 'root'
})
export class QueryCommunicationService {
  // TODO, ideally we want the query to be stored here, and all operatiosn to be performed on the query. Requires some refactoring

  constructor(private userAgentService: UseragentService) { }

  private beforeDisplayQueryRequestedSource = new Subject<AQLQuery>();
  private displayQueryRequestedSource = new Subject<string>();
  displayQueryRequested = this.displayQueryRequestedSource.asObservable();
  beforeDisplayQueryRequested = this.beforeDisplayQueryRequestedSource.asObservable();

  requestDisplayQuery(userAgent: CrudAgent, query: AQLQuery) {
    this.beforeDisplayQueryRequestedSource.next(query);
    this.userAgentService.getSparqlTranslation(userAgent.uuid).subscribe(e => {
      this.displayQueryRequestedSource.next(e);
    });
  }
}
