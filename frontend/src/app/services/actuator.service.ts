import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActuatorService {

  private url = env.BASE_URL() + 'actuator/';

  constructor(private http: HttpClient) {}

  public isPlatformOnline(): Observable<Map<string, string>> {
    return this.http.get<Map<string, string>>(this.url + 'platform', env.HTTP_OPTIONS);
  }
}
