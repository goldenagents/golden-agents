import { TestBed } from '@angular/core/testing';

import { DbagentService } from './dbagent.service';

describe('DbagentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbagentService = TestBed.get(DbagentService);
    expect(service).toBeTruthy();
  });
});
