import { CrudMessage } from '../models/crudmessage';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { environment as env } from '../../environments/environment';
import {AgentResolve} from './agent.service';

@Injectable({
  providedIn: 'root',
})
export class MessageService {

  private static url = env.BASE_API_URL() + 'message';

  constructor(private http: HttpClient) {}

  public agentHistory(agentID: string): Observable<CrudMessage[]> {
    console.log(agentID);
    const result = this.http.get<CrudMessage[]>(this.createURL('agenthistory') + (agentID === undefined ? '' : '?agentID=' + agentID), env.HTTP_OPTIONS);
    result.subscribe(data => {}, e => console.log(e.error));
    return result;
  }

  public preview(agentID: string, messageID: string): Observable<CrudMessage> {
    const result = this.http.get<CrudMessage>(this.createURL('preview') + '?agentID=' + agentID + '&messageID=' + messageID, env.HTTP_OPTIONS);
    result.subscribe(data => {}, e => console.log(e.error));
    return result;
  }

  public performatives(): Observable<Map<string, number>> {
    const result = this.http.get<Map<string, number>>(this.createURL('performatives'), env.HTTP_OPTIONS);
    result.subscribe(data => {}, e => console.log(e.error));
    return result;
  }

  private createURL(method: string) {
    return MessageService.url + '/' + method;
  }
}

@Injectable()
export class PerformativesResolve implements Resolve<Map<string, number>> {

  constructor(private dataservice: MessageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Map<string, number>> {
    return this.dataservice.performatives();
  }
}

@Injectable()
export class AgentMessageHistoryResolve implements Resolve<CrudMessage[]> {

  constructor(private dataservice: MessageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CrudMessage[]> {
    return this.dataservice.agentHistory(route.params['id']);
  }
}
