import { TestBed } from '@angular/core/testing';

import { EmbeddingService } from './embedding.service';

describe('EmbeddingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmbeddingService = TestBed.get(EmbeddingService);
    expect(service).toBeTruthy();
  });
});
