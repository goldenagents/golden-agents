import { TestBed } from '@angular/core/testing';

import { QueryCommunicationService } from './query-communication.service';

describe('QuerySubmitComponentCommunicationServiceService', () => {
  let service: QueryCommunicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QueryCommunicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
