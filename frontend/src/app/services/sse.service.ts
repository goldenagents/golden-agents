import {CrudMessage} from '../models/crudmessage';
import {CrudAgent} from '../models/crudagent';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment as env} from '../../environments/environment';
import {EmbeddingProgress} from '../models/embeddingprogress';
import {QueryProgress} from '../models/queryprogress';
import {UuidService} from './uuid.service';
import {HttpClient} from '@angular/common/http';
import {AQLSuggestions} from '../models/aql/AQLSuggestions';

declare var EventSource;

@Injectable({
  providedIn: 'root',
})
export class SseService {

  private url = env.BASE_API_URL() + 'sse';

  private _newAgentSubs: Observable<CrudAgent>;
  private _killedAgentSubs: Observable<CrudAgent>;
  private _updatedAgentSubs: Observable<CrudAgent>;
  private _readyAgentSubs: Observable<string>;
  private _agentMessageSubs: Map<CrudAgent, Observable<CrudMessage>> = new Map<CrudAgent, Observable<CrudMessage>>();

  private _serverEventSource;

  get newAgents(): Observable<CrudAgent> {
    return this._newAgentSubs;
  }

  get killedAgents(): Observable<CrudAgent> {
    return this._killedAgentSubs;
  }

  get updatedAgents(): Observable<CrudAgent> {
    return this._updatedAgentSubs;
  }

  get readyAgents(): Observable<string> {
    return this._readyAgentSubs;
  }

  public newMessages(agent: CrudAgent): Observable<CrudMessage> {
    return this.observeNewMessages(agent);
  }

  public queryProgress(queryID: string): Observable<QueryProgress> {
    return this.observeQueryProgress(queryID);
  }

  public querySuggestions(agent: CrudAgent): Observable<AQLSuggestions> {
    return this.observeQuerySuggestions(agent);
  }

  public embeddingProgress(agent: CrudAgent): Observable<EmbeddingProgress> {
    return this.observeEmbeddingProgress(agent);
  }

  constructor(private http: HttpClient, private uuidService: UuidService) {
    const clientId = uuidService.applicationInstanceUUID;
    const self = this;

    // Register this client
    this._serverEventSource = new EventSource(this.url + '/register/' + clientId);

    this._serverEventSource.onopen = function () {
      console.log('Event source open');
    };
    this._serverEventSource.onerror = function (event) {
      console.log('Event source error');
      console.log(event);
    };
    this._serverEventSource.onmessage = function (event) {
      console.log(event.id, event.data);

      if (event.id === 'CLOSE') {
        self._serverEventSource.close();
      }
    };

    // Subscribe to each event type
    this.http.get(this.url + '/subscribe/' + clientId + '/agent_create', env.HTTP_OPTIONS).subscribe();
    this.http.get(this.url + '/subscribe/' + clientId + '/agent_update', env.HTTP_OPTIONS).subscribe();
    this.http.get(this.url + '/subscribe/' + clientId + '/agent_delete', env.HTTP_OPTIONS).subscribe();
    this.http.get(this.url + '/subscribe/' + clientId + '/agent_state_ready', env.HTTP_OPTIONS).subscribe();

    // Set up the call-backs
    this._newAgentSubs = new Observable<CrudAgent>(obs => {
      this._serverEventSource.addEventListener('agent_create', event => {
        const agent = CrudAgent.fromEventData(JSON.parse(event.data));
        console.log('Agent created:');
        console.log(agent);
        obs.next(agent);
      });
    });

    this._updatedAgentSubs = new Observable<CrudAgent>(obs => {
      this._serverEventSource.addEventListener('agent_update', event => {
        const agent = CrudAgent.fromEventData(JSON.parse(event.data));
        console.log('Agent updated:');
        console.log(agent);
        obs.next(agent);
      });
    });

    this._killedAgentSubs = new Observable<CrudAgent>(obs => {
      this._serverEventSource.addEventListener('agent_delete', event => {
        const agent = CrudAgent.fromEventData(JSON.parse(event.data));
        console.log('Agent deleted:');
        console.log(agent);
        obs.next(agent);
      });
    });

    this._readyAgentSubs = new Observable<string>(obs => {
      this._serverEventSource.addEventListener('agent_state_ready', event => {
        console.log('Agent ' + event.data + ' ready to do some work');
        obs.next(event.data);
      });
    });
  }

  private observeNewMessages(agent: CrudAgent): Observable<CrudMessage> {

    if (this._agentMessageSubs.has(agent)) {
      return this._agentMessageSubs.get(agent);
    }

    const clientId = this.uuidService.applicationInstanceUUID;

    this.http.get(this.url + '/subscribe/' + clientId + '/messages_' + agent.uuid, env.HTTP_OPTIONS).subscribe();

    const newSub = new Observable<CrudMessage>(obs => {
       this._serverEventSource.addEventListener('messages_' + agent.uuid, event => {
        const message = CrudMessage.fromEventData(JSON.parse(event.data));
        obs.next(message);
      });
    });

    this._agentMessageSubs.set(agent, newSub);
    return newSub;
  }

  private observeQueryProgress(queryID: string) {
    const clientId = this.uuidService.applicationInstanceUUID;
    this.http.get(this.url + '/subscribe/' + clientId + '/query_progress_' + queryID, env.HTTP_OPTIONS).subscribe();
    console.log('Subscribed to query_progress_' + queryID);
    const newSub = new Observable<QueryProgress>(obs => {
      this._serverEventSource.addEventListener('query_progress_' + queryID, event => {
        const progress = QueryProgress.fromEventData(JSON.parse(event.data));
        obs.next(progress);
      });
    });
    return newSub;
  }

  private observeQuerySuggestions(agent: CrudAgent) {
    const clientId = this.uuidService.applicationInstanceUUID;
    this.http.get(this.url + '/subscribe/' + clientId + '/suggestions_' + agent.uuid, env.HTTP_OPTIONS).subscribe();
    console.log('Subscribed to suggestions_' + agent.uuid);
    return new Observable<AQLSuggestions>(obs => {
      this._serverEventSource.addEventListener('suggestions_' + agent.uuid, event => {
        const suggestions = AQLSuggestions.fromEventData(JSON.parse(event.data));
        if (suggestions != null) {
          obs.next(suggestions);
        }
      });
    });
  }

  private observeEmbeddingProgress(agent: CrudAgent) {

    const clientId = this.uuidService.applicationInstanceUUID;
    this.http.get(this.url + '/subscribe/' + clientId + '/embedding_progress_' + agent.uuid, env.HTTP_OPTIONS).subscribe();
    console.log('Subscribed to embedding_progress_' + agent.uuid );
    const newSub = new Observable<EmbeddingProgress>(obs => {
      this._serverEventSource.addEventListener('embedding_progress_' + agent.uuid , event => {
        const progress = EmbeddingProgress.fromEventData(JSON.parse(event.data));
        obs.next(progress);
      });
    });
    return newSub;
  }

  private createURL(method: string) {
    return this.url + '/' + method + '/' + this.uuidService.applicationInstanceUUID;
  }
}
