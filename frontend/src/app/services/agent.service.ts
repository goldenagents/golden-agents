import {CrudAgent} from '../models/crudagent';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {environment as env} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AgentService {

  public static UUID_KEY = 'user-agent-UUID';

  private url = env.BASE_API_URL() + 'agent';

  constructor(private http: HttpClient) {}

  public getAgent(id: string): Observable<CrudAgent> {
    return this.http.get<CrudAgent>(`${this.url}?agentID=${id}`, env.HTTP_OPTIONS);
  }

  /**
   * This method does the same as getAgent, but encapsulates a special case.
   * In case the parameter is null, or if no agent with the given ID exists,
   * the backend will spin up a new user agent.
   * @param id UUID of the user agent, or null if no UUID is yet associated with this browser
   */
  public getUserAgent(id: string | null): Observable<CrudAgent> {
    return this.http.get<CrudAgent>(`${this.url}/user` + (id ? `?agentID=${id}` : ''), env.HTTP_OPTIONS);
  }

  public listAgents(): Observable<CrudAgent[]> {
    const result = this.http.get<CrudAgent[]>(this.createURL('list'), env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => console.log(error));
    return result;
  }

  public listBrokers(): Observable<CrudAgent[]> {
    const result = this.http.get<CrudAgent[]>(this.createURL('brokers'), env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => console.log(error));
    return result;
  }

  public listAgentsInType(agents: CrudAgent[], aType: string) {
    return agents.filter(agent => agent.agentType === aType);
  }

  public getAgentTypes(): Observable<string[]> {
    const result = this.http.get<string[]>(this.createURL('types'), env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => console.log(error));
    return result;
  }

  public deleteAgent(agent: CrudAgent): Observable<CrudAgent> {
    const result = this.http.post<CrudAgent>(this.createURL('kill'), agent, env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => { console.log(error); });
    return result;
  }

  public createAgent(agent: CrudAgent): Observable<CrudAgent> {
    console.log(agent);
    const result = this.http.post<CrudAgent>(this.createURL('create'), agent, env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => { console.log(error); });
    return result;
  }

  public updateAgent(agent: CrudAgent): Observable<CrudAgent> {
    const result = this.http.post<CrudAgent>(this.createURL('update'), agent, env.HTTP_OPTIONS);
    result.subscribe(data => {}, error => { console.log(error); });
    return result;
  }

  private createURL(method: string) {
    return this.url + '/' + method;
  }
}

@Injectable()
export class AgentTypeResolve implements Resolve<string[]> {

  constructor(private dataservice: AgentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string[]> {
    return this.dataservice.getAgentTypes();
  }
}

@Injectable()
export class AgentListResolve implements Resolve<CrudAgent[]> {

  constructor(private dataservice: AgentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CrudAgent[]> {
    return this.dataservice.listAgents();
  }
}

@Injectable()
export class AgentResolve implements Resolve<CrudAgent> {

  constructor(private dataservice: AgentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const id = route.params['id'];
    const obs = this.dataservice.getAgent(id);
    return obs.pipe(
      catchError((err: any) => {
        console.log(err);
        return throwError('Agent not found');
      })
    );
  }
}

@Injectable()
export class UserAgentResolve implements Resolve<CrudAgent> {

  constructor(private dataService: AgentService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const id = route.params['id'] || window.localStorage.getItem('user-agent-UUID');
    const obs = this.dataService.getUserAgent(id);
    return obs.pipe(
      catchError((err: any) => {
        console.log(err);
        return throwError('Failed to fetch or create user agent: ' + err.message);
      })
    );
  }
}
