// Credit goes to wulfsolter for creating this class:
// https://github.com/wulfsolter/angular2-uuid/blob/master/src/uuid.service.ts

import { Injectable } from '@angular/core';
import { UUID } from '../models/uuid';

@Injectable({
  providedIn: 'root',
})
export class UuidService {

  private static app_uuid: string;

  public generateRandomUUID(): string {
    return UUID.UUID();
  }

  get applicationInstanceUUID(): string {
    return UuidService.app_uuid;
  }

  constructor() {
    if (!UuidService.app_uuid) {
      UuidService.app_uuid = UUID.UUID();
    }
  }

}
