import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SparqlResult} from '../models/sparqlresult';
import {Observable} from 'rxjs';
import {environment as env} from '../../environments/environment';
import {QueryRequest} from '../models/queryRequest';
import {CachedQueryResult} from '../models/cachedQueryResult';
import {AQLQuery} from '../models/aql/AQLQuery';
import {AQLTypeItem} from '../models/aql/AQLTypeItem';
import {AQLSuggestions} from '../models/aql/AQLSuggestions';
import {AQLIdentityItem} from '../models/aql/AQLIdentityItem';
import {AQLQueryBlock} from '../models/aql/AQLQueryBlock';

@Injectable({
  providedIn: 'root'
})
export class UseragentService {

  private url = env.BASE_API_URL() + 'agent/user';

  constructor(private http: HttpClient) { }

  public sendQuery(userAgentID: string, query: string, selectedSources: string[], queryType: string): Observable<QueryRequest> {
    const postBody: QueryRequest = new QueryRequest(query, queryType, selectedSources);
    return this.http.post<QueryRequest>(this.url + '/' + userAgentID + '/query', postBody, env.HTTP_OPTIONS);
  }

  public getQueryHistory(userAgentID: string): Observable<CachedQueryResult[]> {
    return this.http.get<CachedQueryResult[]>(this.url + '/' + userAgentID + '/history', env.HTTP_OPTIONS);
  }

  public lastQuery(userAgentID: string): Observable<string> {
    return this.http.get<string>(this.url + '/' + userAgentID + '/lastQuery', env.HTTP_OPTIONS);
  }

  /**
   * Find the ID for the last query for which results have already been collected. This ID can be used
   * in other requests operating on queries or query results that require a queryID.
   *
   * @param userAgentID ID of the user agent who sent the original query request
   */
  public lastQueryID(userAgentID: string) {
    return this.http.get<string>(this.url + '/' + userAgentID + '/lastQueryID', env.HTTP_OPTIONS);
  }

  public getResults(sparqlResult: SparqlResult): Observable<SparqlResult> {
    // TODO what URL is that? We have to move this request in the backend to the user agent controller & service
    // even better TODO, is this even used?
    return this.http.post<SparqlResult>(env.BASE_API_URL() + 'agent/' + sparqlResult, env.HTTP_OPTIONS);
  }
  public getDownloadUrl(userAgentID: string, queryID: string, type: string): string {
    return this.url + '/' + userAgentID + '/' + type.toLowerCase() + '/' + queryID;
  }

  /****************************************
   * START OF AQL RELATED INTERFACES
   * ***************************************/
  public getAQLQuery(userAgentID: string) {
    return this.http.get<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery', env.HTTP_OPTIONS);
  }

  public getAQLQueryJson(userAgentID: string) {
    return this.http.get<AQLQuery>(this.url + '/' + userAgentID + '/aqlqueryjson', env.HTTP_OPTIONS);
  }

  public getQuerySuggestions(userAgentID: string) {
    return new Observable<AQLSuggestions>(obs => {
      this.http.get<AQLSuggestions>(this.url + '/' + userAgentID + '/aqlsuggestions', env.HTTP_OPTIONS).subscribe(suggestion => {
        const suggestions = AQLSuggestions.fromEventData(suggestion);
        if (suggestions != null) {
          obs.next(suggestions);
        }
      });
    });
  }

  public getSparqlTranslation(userAgentID: string): Observable<string> {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.get<string>(this.url + '/' + userAgentID + '/aqlquery/sparqltranslation', {headers, responseType: 'text' as 'json'});
  }

  public addAQLClass(userAgentID: string, classUri: AQLTypeItem) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/addClass',
      {'uri' : classUri.url, 'label' : classUri.label}, env.HTTP_OPTIONS);
  }

  public removeAtFocus(userAgentID: string, focusName: string) {
    let url = this.url + '/' + userAgentID + '/aqlquery/delete';
    if (focusName !== undefined) {
      url += '/' + focusName;
    }
    return this.http.post<AQLQuery>(url, env.HTTP_OPTIONS);
  }

  /**
   * Intersect with a crossing operator of a property at the current focus
   * @param userAgentID   User Agent UUID
   * @param propertyUri      URI of the property to intersect with
   * @param forwards      True iff crossing forwards, false if crossing backwards
   */
  public addAQLProperty(userAgentID: string, propertyUri: AQLTypeItem) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/addProperty/' + propertyUri.forwardjump,
      {'uri' : propertyUri.url, 'label' : propertyUri.label}, env.HTTP_OPTIONS);
  }

  public addAQLEntity(userAgentID: string, entity: AQLTypeItem) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/addEntity', entity, env.HTTP_OPTIONS);
  }

  public addAQLExclude(userAgentID: string) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/exclude', env.HTTP_OPTIONS);
  }
  public addAQLUnion(userAgentID: string) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/union', env.HTTP_OPTIONS);
  }

  public changeAQLFocus(userAgentID: string, focus: string) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/focus/' + focus, env.HTTP_OPTIONS);
  }
  public deleteAQLPart(userAgentID: string, focus: string) {
    return this.http.post<AQLQuery>(this.url + '/' + userAgentID + '/aqlquery/delete/' + focus, env.HTTP_OPTIONS);
  }
}
