import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { CrudAgent } from '../models/crudagent';
import { EmbeddingResult } from '../models/embeddingresult';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmbeddingService {
  
  private url = env.BASE_API_URL() + 'embedding';
  
  constructor(private http: HttpClient) { }
  
  public getNearestNeighbours(agent: CrudAgent, uri: string, k: number): Observable<EmbeddingResult[]> {
    return this.http.get<EmbeddingResult[]>(this.url + '/query/?agentID=' + agent.uuid + '&uri=' + uri + '&k=' + k, env.HTTP_OPTIONS);
  }
}
