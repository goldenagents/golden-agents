import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import {ExpertiseModel} from '../models/ExpertiseModel';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DbagentService {

  private url = env.BASE_API_URL() + 'agent/db';

  constructor(private http: HttpClient) { }

  public getExpertise(id: string): Observable<ExpertiseModel[]> {
    return this.http.get<ExpertiseModel[]>(this.url + '/expertise/?agentID=' + id, env.HTTP_OPTIONS);
  }
}
