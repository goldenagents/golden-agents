import { TestBed } from '@angular/core/testing';

import { UseragentService } from './useragent.service';

describe('UseragentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UseragentService = TestBed.get(UseragentService);
    expect(service).toBeTruthy();
  });
});
