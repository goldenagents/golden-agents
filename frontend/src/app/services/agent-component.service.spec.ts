import { TestBed, inject } from '@angular/core/testing';

import { AgentComponentService } from './agent-component.service';

describe('AgentComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentComponentService]
    });
  });

  it('should be created', inject([AgentComponentService], (service: AgentComponentService) => {
    expect(service).toBeTruthy();
  }));
});
