import { SequenceDiagramComponent } from '../components/message/sequence-diagram/sequence-diagram.component';
import { Injectable, ComponentFactoryResolver, ViewContainerRef, ComponentFactory, ComponentRef} from '@angular/core';

import { CrudAgent } from '../models/crudagent';

import { UserAgentContainerComponent } from '../components/agent/user/user-agent-container/user-agent-container.component';
import { BrokerAgentContainerComponent } from '../components/agent/broker/broker-agent-container/broker-agent-container.component';
import { DfAgentContainerComponent } from '../components/agent/df/df-agent-container/df-agent-container.component';
import { EmbeddingAgentContainerComponent } from '../components/agent/embedding/embedding-agent-container/embedding-agent-container.component';
import { SparqlAgentContainerComponent } from '../components/agent/sparql/sparql-agent-container/sparql-agent-container.component';
import {AgentDirective} from '../components/agent/_container/agent.directive';

@Injectable()
export class AgentComponentService {

  private _agentContainer: AgentDirective;
  private _componentFactories: Map<string, ComponentFactory<any>>;

  constructor(private factoryResolver: ComponentFactoryResolver) {
    this._componentFactories = new Map<string, ComponentFactory<any>>();
    this._componentFactories['User'] = this.factoryResolver.resolveComponentFactory(UserAgentContainerComponent);
    this._componentFactories['Broker'] = this.factoryResolver.resolveComponentFactory(BrokerAgentContainerComponent);
    this._componentFactories['DF'] = this.factoryResolver.resolveComponentFactory(DfAgentContainerComponent);
    this._componentFactories['DB'] = this.factoryResolver.resolveComponentFactory(SparqlAgentContainerComponent);
    this._componentFactories['Embedding'] = this.factoryResolver.resolveComponentFactory(EmbeddingAgentContainerComponent);
  }

  set agentContainer(agentContainer: AgentDirective) {
    this._agentContainer = agentContainer;
  }

  get agentContainer(): AgentDirective {
    return this._agentContainer;
  }

  public addDynamicAgentComponents(agent: CrudAgent) {
    if (this.agentContainer) {

      this.agentContainer.viewContainerRef.clear();

      const componentRef: ComponentRef<any> =
        this.agentContainer.viewContainerRef.createComponent(this._componentFactories[agent.agentType]);
    } else {
      console.error('Parent containers not set');
    }
  }

}
