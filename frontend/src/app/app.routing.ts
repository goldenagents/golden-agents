import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/error/page-not-found/page-not-found.component';
import { AgentContainerComponent} from './components/agent/_container/agent-container.component';
import { DashboardComponent } from './components/dashboard/container/dashboard.component';
import {AgentListResolve, AgentTypeResolve, AgentResolve, UserAgentResolve} from './services/agent.service';
import { AgentMessageHistoryResolve, PerformativesResolve } from './services/message.service';

const routes: Routes = [
  {
    // If no path is given, the user should be sent to the query page.
    // The UserAgentResolve will try to request a previously used UserAgent,
    // but accept a new one if that agent does not exist
    path: '',
    component: AgentContainerComponent,
    resolve:
      {
        agent: UserAgentResolve,
        agents: AgentListResolve,
        performatives: PerformativesResolve
      },
    pathMatch: 'full'
  },
  { // Show the dashboard
    path: 'dashboard',
    component: DashboardComponent,
    resolve:
      {
        agents: AgentListResolve,
        agentTypes: AgentTypeResolve
      }
  },

  { // If no tab is given, just default to the info tab
    path: 'agent/:id',
    redirectTo: 'agent/:id/info',
    pathMatch: 'full'
  },
  { // Show the agent page
    path: 'agent/:id/:tab',
    component: AgentContainerComponent,
    resolve:
      {
        agent: AgentResolve,
        agents: AgentListResolve,
        messageHistory: AgentMessageHistoryResolve,
        performatives: PerformativesResolve
      }
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

export const Routing: ModuleWithProviders<any> = RouterModule.forRoot(routes, { enableTracing: false });
