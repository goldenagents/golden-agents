import { UuidService } from './services/uuid.service';
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActuatorService } from './services/actuator.service';
import { SpinnerOverlayService } from './services/spinner-overlay.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  defaultTitle = 'Golden Agents Framework';

  public constructor(
    private titleService: Title,
    private uuidService: UuidService,
    private actuatorService: ActuatorService,
    private overlayService: SpinnerOverlayService,
    private router: Router) {

    this.titleService.setTitle(this.defaultTitle);
    console.log('Starting client with ID ' + this.uuidService.applicationInstanceUUID);

    this.actuatorService.isPlatformOnline().subscribe (
      () => this.checkBackendEveryOnceInAWhile(),
      () => this.showSpinnerUntilBackendIsOnline()
    );
  }

  /**
   * On bootup, we check every second
   */
  private showSpinnerUntilBackendIsOnline() {
    const interval = setInterval(() => {

      this.actuatorService.isPlatformOnline().subscribe (
        () => {
          clearInterval(interval);
          this.overlayService.hide();
          this.refreshPage();
          this.checkBackendEveryOnceInAWhile();
        },
        () => this.overlayService.show('Waiting for backend')
      );

    }, 1000);
  }

  /**
   * During normal operation, we check every minute
   */
  private checkBackendEveryOnceInAWhile() {
    setInterval(() => {

      this.actuatorService.isPlatformOnline().subscribe (
        () => {
          console.log('backend is online');
          this.overlayService.hide();
        },
        () => this.overlayService.show('Waiting for backend')
      );
    }, 60 * 1000);
  }

  private refreshPage() {
    this.router.navigateByUrl(
      '/RefreshComponent',
      { skipLocationChange: true }
      ).then(
        () => this.router.navigate(['']
        )
    );
  }

}
