
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AgentModule } from './agent.module';

import { BrokerAgentSettingsComponent } from '../components/agent/broker/broker-agent-settings/broker-agent-settings.component';
import { BrokerAgentMessagesComponent } from '../components/agent/broker/broker-agent-messages/broker-agent-messages.component';
import { BrokerAgentContainerComponent } from '../components/agent/broker/broker-agent-container/broker-agent-container.component';

@NgModule({
  declarations: [
    BrokerAgentSettingsComponent,
    BrokerAgentMessagesComponent,
    BrokerAgentContainerComponent
  ],
  imports: [
    AgentModule,
  ],
})
export class AgentBrokerModule {}
