
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AgentModule } from './agent.module';

import { EmbeddingAgentBootComponent } from '../components/agent/embedding/embedding-agent-boot/embedding-agent-boot.component';
import { EmbeddingAgentSettingsComponent } from '../components/agent/embedding/embedding-agent-settings/embedding-agent-settings.component';
import { EmbeddingAgentMessagesComponent } from '../components/agent/embedding/embedding-agent-messages/embedding-agent-messages.component';
import { EmbeddingAgentContainerComponent } from '../components/agent/embedding/embedding-agent-container/embedding-agent-container.component';
import { EmbeddingAgentQueryComponent } from '../components/agent/embedding/embedding-agent-query/embedding-agent-query.component';

@NgModule({
  declarations: [
    EmbeddingAgentSettingsComponent,
    EmbeddingAgentMessagesComponent,
    EmbeddingAgentContainerComponent,
    EmbeddingAgentBootComponent,
    EmbeddingAgentQueryComponent
  ],
  imports: [
    AgentModule,
  ],
})
export class AgentEmbeddingModule {}
