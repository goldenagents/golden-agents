
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { Routing } from '../app.routing';
import { MessageModule } from './message.module';
import { AgentContainerComponent } from '../components/agent/_container/agent-container.component';

import { AgentComponentService } from '../services/agent-component.service';
import {AgentService, AgentResolve, UserAgentResolve} from '../services/agent.service';

import { AppBootstrapModule } from '../app-bootstrap.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { LineChartComponent } from '../components/misc/line-chart/line-chart.component';
import { ProgressBarComponent } from '../components/misc/progress-bar/progress-bar.component';
import { RadialBarComponent } from '../components/misc/radial-bar/radial-bar.component';
import {AgentDirective} from '../components/agent/_container/agent.directive';

@NgModule({
  // The components, directives, and pipes that belong to this NgModule.
  declarations: [
    AgentContainerComponent,
    LineChartComponent,
    ProgressBarComponent,
    RadialBarComponent,
    AgentDirective
  ],
  // The subset of declarations that should be visible and usable in the component templates of other NgModules.
  exports: [
    MessageModule,
    FormsModule,
    MatTabsModule,
    HttpClientModule,
    CommonModule,
    LineChartComponent,
    ProgressBarComponent,
    RadialBarComponent
  ],
  // Other modules whose exported classes are needed by component templates declared in this NgModule.
  imports: [
    CommonModule,
    MessageModule,
    RouterModule,
    Routing,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    AppBootstrapModule
  ],
  // Creators of services that this NgModule contributes to the global collection of services; they become accessible in all parts of the application
  providers: [
    AgentService,
    AgentResolve,
    UserAgentResolve,
    AgentComponentService,
    NgbActiveModal
  ],
})
export class AgentModule {}
