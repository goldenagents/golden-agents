
import { NgModule } from '@angular/core';
import { AgentModule } from './agent.module';

import { SparqlAgentSettingsComponent } from '../components/agent/sparql/sparql-agent-settings/sparql-agent-settings.component';
import { SparqlAgentMessagesComponent } from '../components/agent/sparql/sparql-agent-messages/sparql-agent-messages.component';
import { SparqlAgentStatisticsComponent } from '../components/agent/sparql/sparql-agent-statistics/sparql-agent-statistics.component';
import { SparqlAgentContainerComponent } from '../components/agent/sparql/sparql-agent-container/sparql-agent-container.component';

@NgModule({
    declarations: [
        SparqlAgentSettingsComponent,
        SparqlAgentMessagesComponent,
        SparqlAgentStatisticsComponent,
        SparqlAgentContainerComponent
    ],
    imports: [
        AgentModule,
    ],
    exports: [
        SparqlAgentStatisticsComponent
    ]
})
export class AgentSparqlModule {}
