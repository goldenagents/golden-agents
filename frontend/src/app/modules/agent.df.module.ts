
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AgentModule } from './agent.module';

import { DfAgentSettingsComponent } from '../components/agent/df/df-agent-settings/df-agent-settings.component';
import { DfAgentMessagesComponent } from '../components/agent/df/df-agent-messages/df-agent-messages.component';
import { DfAgentContainerComponent } from '../components/agent/df/df-agent-container/df-agent-container.component';

@NgModule({
  declarations: [
    DfAgentSettingsComponent,
    DfAgentMessagesComponent,
    DfAgentContainerComponent
  ],
  imports: [
    AgentModule,
  ],
})
export class AgentDfModule {}
