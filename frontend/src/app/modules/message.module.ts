import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MessageComposeFormComponent } from '../components/message/message-compose-form/message-compose-form.component';
import { MessageService, AgentMessageHistoryResolve, PerformativesResolve } from '../services/message.service';
import { SseService } from '../services/sse.service';

// TODO D3 service was removed
// import { D3Service } from 'd3-ng2-service';
import { SequenceDiagramComponent } from '../components/message/sequence-diagram/sequence-diagram.component';
import { MapToIterable } from '../pipes/map-iterable.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    MessageComposeFormComponent,
    SequenceDiagramComponent,
    MapToIterable
  ],
  exports: [
    MessageComposeFormComponent,
    SequenceDiagramComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    MessageService,
    SseService,
    // TODO D3 service was removed
    // D3Service,
    AgentMessageHistoryResolve,
    PerformativesResolve,
  ],
})
export class MessageModule {}
