import {NgModule} from '@angular/core';
import {AgentModule} from './agent.module';

import {UserAgentContainerComponent} from '../components/agent/user/user-agent-container/user-agent-container.component';
import {QueryInfoComponent} from '../components/query/query-info/query-info.component';
import {QueryResultComponent} from '../components/query/query-result/query-result.component';
import {QuerySubmitComponent} from '../components/query/query-submit/query-submit.component';

import {MatSelectModule} from '@angular/material/select';
import {CodemirrorModule} from '@ctrl/ngx-codemirror';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AgentSparqlModule} from './agent.sparql.module';
import {MatPseudoCheckboxModule} from '@angular/material/core';
import {SparklisSubmitGaUiComponent} from '../components/query/sparklis-submit-ga-ui/sparklis-submit-ga-ui.component';
import {ResultDataTableComponent} from '../components/misc/result-data-table/result-data-table.component';

@NgModule({
  declarations: [
    UserAgentContainerComponent,
    QueryInfoComponent,
    QueryResultComponent,
    QuerySubmitComponent,
    SparklisSubmitGaUiComponent,
    ResultDataTableComponent
  ],
  imports: [
    AgentModule,
    MatSelectModule,
    BrowserModule,
    FormsModule,
    CodemirrorModule,
    AgentSparqlModule,
    MatPseudoCheckboxModule
  ],
})
export class AgentUserModule {}
