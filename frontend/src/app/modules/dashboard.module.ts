
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SseService } from '../services/sse.service';
import { AgentService, AgentListResolve, AgentTypeResolve } from '../services/agent.service';

import { AgentGridItemComponent } from '../components/dashboard/agent-grid-item/agent-grid-item.component';
import { AgentGridComponent } from '../components/dashboard/agent-grid/agent-grid.component';
import { AgentCreationFormComponent } from '../components/dashboard/create-form/creation-form.component';
import { DashboardComponent } from '../components/dashboard/container/dashboard.component';

import { SparqlAgentCreateFieldsComponent } from '../components/dashboard/create-fields/sparql-agent-create-fields/sparql-agent-create-fields.component';
import { UserAgentCreateFieldsComponent } from '../components/dashboard/create-fields/user-agent-create-fields/user-agent-create-fields.component';
import { BrokerAgentCreateFieldsComponent } from '../components/dashboard/create-fields/broker-agent-create-fields/broker-agent-create-fields.component';
import { EmbeddingAgentCreateFieldsComponent } from '../components/dashboard/create-fields/embedding-agent-create-fields/embedding-agent-create-fields.component';


import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule} from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { AgentTypeFilterPipe } from '../pipes/agent-type-filter.pipe';
import {SpinnerOverlayComponent} from '../components/dashboard/overlay/spinner-overlay/spinner-overlay.component';
import {SpinnerComponent} from '../components/dashboard/overlay/spinner/spinner.component';
import {SpinnerOverlayService} from '../services/spinner-overlay.service';


@NgModule({
  declarations: [
    AgentGridItemComponent,
    AgentGridComponent,
    AgentCreationFormComponent,
    DashboardComponent,
    SparqlAgentCreateFieldsComponent,
    UserAgentCreateFieldsComponent,
    BrokerAgentCreateFieldsComponent,
    EmbeddingAgentCreateFieldsComponent,
    AgentTypeFilterPipe,
    SpinnerOverlayComponent,
    SpinnerComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatGridListModule,
    MatIconModule,
    MatTabsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
  ],
  exports: [],
  providers: [
    SseService,
    AgentService,
    AgentListResolve,
    AgentTypeResolve,
    SpinnerOverlayService
  ],
})
export class DashboardModule {}
