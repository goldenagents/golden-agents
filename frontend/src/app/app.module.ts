import {AgentUserModule} from './modules/agent.user.module';
import {AgentDfModule} from './modules/agent.df.module';
import {AgentSparqlModule} from './modules/agent.sparql.module';
import {AgentBrokerModule} from './modules/agent.broker.module';
import {AgentEmbeddingModule} from './modules/agent.embedding.module';
import {DashboardModule} from './modules/dashboard.module';

import {NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser'; // When you want to run your app in a browser
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http'; // When you to talk to a server
import {FormsModule} from '@angular/forms'; // When building reactive forms
import {AppComponent} from './app.component'; // The main component of this app
import {Routing} from './app.routing';

import {PageNotFoundComponent} from './components/error/page-not-found/page-not-found.component';
import {UuidService} from './services/uuid.service';

import {NgbCollapseModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    DashboardModule,
    AgentUserModule,
    AgentSparqlModule,
    AgentBrokerModule,
    AgentEmbeddingModule,
    AgentDfModule,
    Routing,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgbCollapseModule
  ],
  providers: [
    Title,
    UuidService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
