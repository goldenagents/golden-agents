import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageSendFormComponent } from './message-send-form.component';

describe('MessageSendFormComponent', () => {
  let component: MessageSendFormComponent;
  let fixture: ComponentFixture<MessageSendFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageSendFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageSendFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
