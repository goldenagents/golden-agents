import { CrudAgent } from '../../../models/crudagent';
import { DummyMessage } from '../../../models/dummymessage';
import { CrudMessage } from '../../../models/crudmessage';
import { MessageService } from '../../../services/message.service';
import { Component, OnInit, Injectable, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-message-compose-form',
  templateUrl: './message-compose-form.component.html',
  styleUrls: ['./message-compose-form.component.css']
})
@Injectable()
export class MessageComposeFormComponent implements OnInit {

  @Input() senderAgent: CrudAgent;
  @Input() performatives: Map<string, number>;
  @Input() receiverAgents: CrudAgent[];

  message: DummyMessage;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
    this.message = new DummyMessage(
      this.senderAgent.uuid,
      this.senderAgent.host,
      this.senderAgent.port,
      this.senderAgent.nickname
    );
    console.log(this.message);
  }

  trackByPerfIdx(index, item) {
    return this.performatives.get(item);
  }

  performativeKeys(): string[] {
    return Object.keys(this.performatives);
  }

  public onReceiverSelect(selection): void {
    const receiver = this.receiverAgents[selection[0]];
    this.message.receiverHost = receiver.host;
    this.message.receiverPort = receiver.port;
    this.message.receiverUUID = receiver.uuid;
    this.message.receiverNickname = receiver.nickname;
  }

  sendMessage(): void {
    this.activeModal.close(this.message);
  }
}
