import {CrudMessage} from '../../../models/crudmessage';
import {SequenceDiagramComponent} from './sequence-diagram.component';

export class SequenceMessage {

  private _arrowY: number;
  private _labelY: number;
  private _messageX: number;
  private _lineX1: number;
  private _lineX2: number;
  private _timestampX: number;

  constructor(public message: CrudMessage, private diagramComponent: SequenceDiagramComponent) {
    const index = this.diagramComponent.messages.indexOf(message);
    const classSpace = this.diagramComponent.HOR_SPACE / (this.diagramComponent.agentsList.length - 1);
    this.updateMessageValues(index, classSpace);
  }

  public updateMessageValues(index: number, classSpace: number): void {
    const senderIndex = this.diagramComponent.agentsList.indexOf(this.message.senderUUID);
    const receiverIndex = this.diagramComponent.agentsList.indexOf(this.message.receiverUUID);

    this._arrowY = this.diagramComponent.MESSAGE_ARROW_Y_OFFSET + (index * this.diagramComponent.MESSAGE_SPACE);
    this._labelY = this.diagramComponent.MESSAGE_LABEL_Y_OFFSET + (index * this.diagramComponent.MESSAGE_SPACE);
    this._messageX = this.diagramComponent.XMARGIN + this.diagramComponent.PAD_LEFT + this.diagramComponent.MESSAGE_LABEL_X_OFFSET +
      (((receiverIndex - senderIndex) * classSpace) / 2) +
      (senderIndex * classSpace);
    this._lineX1 = this.diagramComponent.PAD_LEFT + (senderIndex * classSpace) + this.diagramComponent.XMARGIN;
    this._lineX2 = this.diagramComponent.PAD_LEFT + (receiverIndex * classSpace) + this.diagramComponent.XMARGIN;
    this._timestampX = this.diagramComponent.PAD_LEFT + this.diagramComponent.MESSAGE_LABEL_X_OFFSET;
  }

  public arrowY() {
    return this._arrowY;
  }

  public labelY() {
    return this._labelY;
  }

  public messageX() {
    return this._messageX;
  }

  public lineX1(): number {
    return this._lineX1;
  }

  public lineX2(): number {
    return this._lineX2;
  }

  public timestampX(): number {
    return this._timestampX;
  }
}
