import {CrudAgent} from '../../../models/crudagent';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  QueryList,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewChildren
} from '@angular/core';

import {CrudMessage} from '../../../models/crudmessage';
import {ActivatedRoute, Router} from '@angular/router';
import {SseService} from '../../../services/sse.service';
import {NgbPopover} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs';
import {SequenceMessage} from './sequenceMessage';

@Component({
  selector: 'app-message-sequence-diagram',
  templateUrl: './sequence-diagram.component.html',
  styleUrls: ['./sequence-diagram.component.css'],
})
export class SequenceDiagramComponent implements OnInit, OnDestroy {
  public readonly XMARGIN = 30;
  public readonly PAD_LEFT = 100;
  public readonly PAD_TOP = 0;
  public readonly WIDTH = 950;
  public readonly HOR_SPACE = this.WIDTH - (this.PAD_LEFT * 2);
  public readonly MESSAGE_SPACE = 30;
  public readonly MESSAGE_LABEL_X_OFFSET = -40;
  public readonly MESSAGE_LABEL_Y_OFFSET = 75;
  public readonly MESSAGE_ARROW_Y_OFFSET = 80;
  public readonly CLASS_LABEL_SIZE = 14;
  public readonly CLASS_LABEL_PADDING = 10;

  private readonly colors = ['#e6194B', '#3cb44b', '#ffe119', '#4363d8',
    '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#fabebe',
    '#008080', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3',
    '#808000', '#ffd8b1', '#000075', '#a9a9a9'];

  public CLASS_SPACE: number;

  private newMessageSubscription: Subscription;
  public messages: CrudMessage[] = [];
  public agents: Map<String, String> = new Map();
  public agentsList: string[] = [];
  private currentColorIndex = 0;

  public messageDiagramInfo: SequenceMessage[] = [];

  private scrollContainer: HTMLElement;

  /** A field that maps agents to a tuple of numbers, indicating the first index of a message they appear in, and the last **/
  public agentMessageLimits = new Map<string, [number, number]>();

  /** A field that maps conversationID's to colour values **/
  public conversations = new Map<string, string>();
  public conversationIds: string[] = [];

  @Input()
  private agent: CrudAgent;
  @ViewChild('messageHistoryDiagramContainer', {static: false}) scrollContainerRef: ElementRef;

  constructor (
    private changeDetector: ChangeDetectorRef,
    private sse: SseService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.messages = data.messageHistory as CrudMessage[];
      this.messages.forEach(m => {
        this.updateMessage(m);
      });

      this.newMessageSubscription = this.sse.newMessages(this.agent).subscribe(newMessages => {
        this.messages = this.messages.concat(newMessages);
        this.updateMessage(newMessages);
        this.updateScroll();
      });
    });

    this.scrollContainer = this.scrollContainerRef.nativeElement as HTMLElement;
 }

  updateMessage(message: CrudMessage): void {
    if (this.findUnknownClasses([message])) {
      this.signalClassSpaceChanged();
    }
    this.messageDiagramInfo.push(new SequenceMessage(message, this));

    if (!this.conversations.has(message.conversationID)) {
      this.conversations.set(message.conversationID, this.colors[this.currentColorIndex]);
      this.conversationIds.push(message.conversationID);
      this.currentColorIndex++;
      if (this.currentColorIndex >= this.colors.length) {
        this.currentColorIndex = 0;
      }
    }

    this.changeDetector.markForCheck();
  }

  private signalClassSpaceChanged(): void {
    const classSpace = this.HOR_SPACE / (this.agentsList.length - 1);
    this.messageDiagramInfo.forEach((message, index) => {
      message.updateMessageValues(index, classSpace);
    });
  }

  private updateMessageLimit(agentID: string) {
      if (!this.agentMessageLimits.has(agentID)) {
         this.agentMessageLimits.set(agentID, [this.messageDiagramInfo.length, this.messageDiagramInfo.length]);
      }

      const agentLimits = this.agentMessageLimits.get(agentID);
      agentLimits[1] = this.messageDiagramInfo.length;
      this.agentMessageLimits.set(agentID, agentLimits);
  }

  ngOnDestroy(): void {
    if (this.newMessageSubscription) {
      this.newMessageSubscription.unsubscribe();
    }
  }

  public navigateToAgent(agentID: string): void {
    this.router.navigate(['/agent', agentID, 'messages']);
  }

  /**
   * Update the scroll position of the SVG container to show to last entry, unless
   * the user has scroll up manually
   */
  private updateScroll() {
    const autoScrollEnabled = (this.scrollContainer.scrollHeight - this.scrollContainer.clientHeight) <=
      (this.scrollContainer.scrollTop + 50);
    if (autoScrollEnabled) {
      this.scrollContainer.scrollTop = this.scrollContainer.scrollHeight;
    }
  }

  /**
   * Check if new agents, which should be drawn as classes on the top, have appeared since the most
   * recent addition of messages. This would require redrawing from the start
   */
  private findUnknownClasses(messages: CrudMessage[]): boolean {
    const agents = this.agents;
    const newAgents = new Map();
    let foundAny = false;
    let agentList = this.agentsList;
    const self = this;
      messages.forEach(function(m: CrudMessage, i: number) {
        self.updateMessageLimit(m.senderUUID);
        self.updateMessageLimit(m.receiverUUID);

        if (!agents.has(m.senderUUID)) {
          newAgents.set(m.senderUUID, m.senderNickname);
          agents.set(m.senderUUID, m.senderNickname);
          agentList = agentList.concat(m.senderUUID);
          foundAny = true;
        }
        if (!agents.has(m.receiverUUID)) {
          newAgents.set(m.receiverUUID, m.receiverNickname);
          agents.set(m.receiverUUID, m.receiverNickname);
          agentList = agentList.concat(m.receiverUUID);
          foundAny = true;
        }
      });
    this.agents = agents;
    this.agentsList = agentList;

    if (foundAny) {
      this.CLASS_SPACE = this.HOR_SPACE / (this.agentsList.length - 1);
    }

    return foundAny;
  }

}
