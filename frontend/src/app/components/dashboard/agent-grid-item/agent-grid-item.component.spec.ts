import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentGridItemComponent } from './agent-grid-item.component';

describe('AgentGridItemComponent', () => {
  let component: AgentGridItemComponent;
  let fixture: ComponentFixture<AgentGridItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentGridItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGridItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
