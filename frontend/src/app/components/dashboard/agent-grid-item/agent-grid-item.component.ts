import { CrudAgent } from '../../../models/crudagent';
import { AgentService } from '../../../services/agent.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-agent-grid-item',
  templateUrl: './agent-grid-item.component.html',
  styleUrls: ['./agent-grid-item.component.css'],
})
export class AgentGridItemComponent implements OnInit {

  popoverTitle = 'Are you sure?';
  popoverMessage = 'Agents cannot be unkilled';

  @Input() agent: CrudAgent;

  constructor(private agentService: AgentService) {}

  ngOnInit() {}

  routeAgentKill(agent: CrudAgent): void {
    this.agentService.deleteAgent(agent);
  }
}
