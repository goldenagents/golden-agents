import { Component, OnInit, OnDestroy } from '@angular/core';
import { AgentService } from '../../../services/agent.service';
import { CrudAgent } from '../../../models/crudagent';
import { ActivatedRoute } from '@angular/router';
import { SseService } from '../../../services/sse.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-agent-grid',
  templateUrl: './agent-grid.component.html',
  styleUrls: ['./agent-grid.component.css'],
})
export class AgentGridComponent implements OnInit, OnDestroy {

  private newAgentSubscription: Subscription;
  private killedAgentSubscription: Subscription;
  private updatedAgentSubscription: Subscription;
  private readyAgentSubscription: Subscription;

  agents: CrudAgent[];
  agentTypes: string[];

  constructor(
    private sseService: SseService,
    private route: ActivatedRoute,
    private agentService: AgentService) {
     agentService.getAgentTypes().subscribe(aTypes => {
        this.agentTypes = aTypes;
     });
  }

  getAgents(aType: string) {
    return this.agents.filter(agent => agent.agentType === aType);
  }

  ngOnInit() {
    // Get resolved data
    this.route.data.subscribe(data => {
      this.agents = data.agents as CrudAgent[];
    });

    this.newAgentSubscription = this.sseService.newAgents.subscribe(newAgent => {
      this.agents.push(newAgent);
    });

    this.killedAgentSubscription = this.sseService.killedAgents.subscribe(killedAgent => {
      this.agents = this.agents.filter(agent => agent.uuid !== killedAgent.uuid);
    });

    this.updatedAgentSubscription = this.sseService.updatedAgents.subscribe(updatedAgent => {
      const index = this.agents.findIndex(agent => agent.uuid === updatedAgent.uuid);
      this.agents[index] = updatedAgent;
    });

    this.readyAgentSubscription = this.sseService.readyAgents.subscribe(readyAgentUUID => {
      console.log('Agent ' + readyAgentUUID + ' ready to do some work');
      const index = this.agents.findIndex(agent => agent.uuid === readyAgentUUID);
      this.agents[index].ready = true;
    });
  }

  ngOnDestroy() {
    if (this.newAgentSubscription) {
      this.newAgentSubscription.unsubscribe();
    }
    if (this.killedAgentSubscription) {
      this.killedAgentSubscription.unsubscribe();
    }
    if (this.updatedAgentSubscription) {
      this.updatedAgentSubscription.unsubscribe();
    }
  }
}
