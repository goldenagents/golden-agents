import {CrudAgent} from '../../../models/crudagent';
import {Component, Injectable, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {DummyAgent} from '../../../models/dummyagent';
import {AgentService} from '../../../services/agent.service';
import {BrokerAgentCreateFieldsComponent} from '../create-fields/broker-agent-create-fields/broker-agent-create-fields.component';
import {SparqlAgentCreateFieldsComponent} from '../create-fields/sparql-agent-create-fields/sparql-agent-create-fields.component';
import {UserAgentCreateFieldsComponent} from '../create-fields/user-agent-create-fields/user-agent-create-fields.component';
import {EmbeddingAgentCreateFieldsComponent} from '../create-fields/embedding-agent-create-fields/embedding-agent-create-fields.component';
import {ActivatedRoute} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-agent-create-form',
  templateUrl: './creation-form.component.html',
  styleUrls: ['./creation-form.component.css'],
})

@Injectable()
export class AgentCreationFormComponent implements OnInit {

  agentTypes: string[];
  model: DummyAgent;
  agentsInfo = new Map<string, { [name: string]: string }>();
  closeResult: string;

  @ViewChild('createForm', {static: false}) createFormContainer: ViewContainerRef;

  constructor(private service: AgentService,
    private route: ActivatedRoute,
    private modalService: NgbModal) {}

  ngOnInit() {
    // Get resolved data
    this.route.data.subscribe(data => {
      this.agentTypes = data.agentTypes;
      const emptyMap: { [name: string]: string } =  {};
      this.model = new DummyAgent(this.agentTypes[0], this.agentTypes[0], emptyMap);
    });
    this.fillMaps();
  }

  open() {
    switch (this.model.agentType) {
        case 'DB':
          this.modalService.open(SparqlAgentCreateFieldsComponent);
          break;
        case 'User':
          this.modalService.open(UserAgentCreateFieldsComponent);
          break;
        case 'Broker':
          this.modalService.open(BrokerAgentCreateFieldsComponent);
          break;
        case 'Embedding':
          this.modalService.open(EmbeddingAgentCreateFieldsComponent);
          break;
      }
  }

  private fillMaps() {
    const mapEcartico: { [name: string]: string } =  {};
    mapEcartico['rdfDataURI'] = 'https://api.druid.datalegend.net/datasets/Jauco/golden-agents-ecartico/services/sparql/sparql';
    mapEcartico['mappingFilename'] = 'configs/Ecartico2GA.trig';
    mapEcartico['queryPageSize'] = '10000';
    mapEcartico['icon'] = 'dns';
    mapEcartico['type'] = 'DB';

    const mapOnstage: { [name: string]: string } =  {};
    mapOnstage['rdfDataURI'] = 'https://api.druid.datalegend.net/datasets/Jauco/golden-agents-onstage/services/golden-agents-stcn/sparql';
    mapOnstage['mappingFilename'] = 'configs/OnStage2GA.trig';
    mapOnstage['queryPageSize'] = '10000';
    mapOnstage['icon'] = 'dns';
    mapOnstage['type'] = 'DB';

    const mapSTCN: { [name: string]: string } =  {};
    mapSTCN['rdfDataURI'] = 'dumps/STCN';
    mapSTCN['mappingFilename'] = 'configs/STCN2GA.trig';
    mapSTCN['queryPageSize'] = '10000';
    mapSTCN['icon'] = 'dns';
    mapSTCN['type'] = 'DB';

    const mapBroker: { [name: string]: string } =  {};
    mapBroker['ontology'] = 'configs/GA_Ontology.owl';
    mapBroker['linkset'] = 'configs/LinksetOnstage.nt,configs/LinksetSTCNOnstage.ttl';
    mapBroker['icon'] = 'call_split';
    mapBroker['type'] = 'Broker';

    const mapUser: { [name: string]: string } =  {};
    mapUser['icon'] = 'person';
    mapUser['type'] = 'User';

    const mapDF: { [name: string]: string } =  {};
    mapDF['icon'] = 'import_contacts';
    mapDF['type'] = 'DF';

    const mapEmbedding: { [name: string]: string } =  {};
    mapEmbedding['file'] = 'SmallTest4.nt';
    mapEmbedding['alpha'] = '0.1';
    mapEmbedding['epsilon'] = '0.0001';
    mapEmbedding['icon'] = 'dns';
    mapEmbedding['type'] = 'Embedding';
    mapEmbedding['sparqlEndpointUrl'] = 'https://api.druid.datalegend.net/datasets/Jauco/golden-agents-onstage/services/golden-agents-stcn/sparql';

    this.agentsInfo.set('Ecartico', mapEcartico);
    this.agentsInfo.set('STCN', mapSTCN);
    this.agentsInfo.set('Onstage', mapOnstage);
    this.agentsInfo.set('User', mapUser);
    this.agentsInfo.set('Broker', mapBroker);
    this.agentsInfo.set('DF', mapDF);
    // this.agentsInfo.set('Embedding Onstage', mapEmbedding);
  }

  private createAll() {
    this.agentsInfo.forEach((key, value) => {
      this.createAgent(new DummyAgent(key['type'], value, key));
    });
  }

  private createAgent(dummy: DummyAgent) {
    this.service.createAgent(CrudAgent.fromDummy(dummy));
  }

  /**
   * When the user selects a different agent type we update the
   * nickname if it's one of the default nicknames.
   */
  onTypeSelect(newType: string): void {
    if (this.agentTypes.includes(this.model.nickname)) {
      this.model.agentType = newType;
      this.model.nickname = newType;
      this.model.additionals = this.agentsInfo[newType];
    }
  }
}
