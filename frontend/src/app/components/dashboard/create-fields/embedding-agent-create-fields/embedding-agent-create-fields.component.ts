import { CrudAgent } from '../../../../models/crudagent';
import { DummyAgent } from '../../../../models/dummyagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-embedding-agent-create-fields',
  templateUrl: './embedding-agent-create-fields.component.html',
  styleUrls: ['./embedding-agent-create-fields.component.css']
})
export class EmbeddingAgentCreateFieldsComponent implements OnInit {

  agent: DummyAgent;

  constructor(private service: AgentService, private ngbActiveModal: NgbActiveModal) { }

   ngOnInit() {
     const emptyMap: { [name: string]: string } =  {};
     emptyMap['icon'] = 'dns';
     emptyMap['alpha'] = '1e-4';
     emptyMap['epsilon'] = '1e-8';
     emptyMap['dimensions'] = '50';
     emptyMap['tolerance'] = '1e-6';
     emptyMap['literals'] = 'true';
     emptyMap['normalize'] = 'true';
     emptyMap['reverse'] = 'false';
     emptyMap['bcaThreads'] = '4';
     emptyMap['maxIterations'] = '1000';
     emptyMap['optimizationAlgorithm'] = 'Adam';
     emptyMap['xMax'] = '100';
     emptyMap['gdThreads'] = '4';
     this.agent = new DummyAgent('Embedding', 'Embedding', emptyMap);
   }

  onSubmit() {
    this.createAgent(this.agent);
    this.closeModal('Create Click');
  }

  private createAgent(dummy: DummyAgent) {
    this.service.createAgent(CrudAgent.fromDummy(dummy));
  }

  closeModal(reason: string) {
    this.ngbActiveModal.close(reason);
  }
}
