import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentCreateFieldsComponent } from './embedding-agent-create-fields.component';

describe('EmbeddingAgentCreateFieldsComponent', () => {
  let component: EmbeddingAgentCreateFieldsComponent;
  let fixture: ComponentFixture<EmbeddingAgentCreateFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentCreateFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentCreateFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
