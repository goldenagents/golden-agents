import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparqlAgentCreateFieldsComponent } from './sparql-agent-create-fields.component';

describe('SparqlAgentCreateFieldsComponent', () => {
  let component: SparqlAgentCreateFieldsComponent;
  let fixture: ComponentFixture<SparqlAgentCreateFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparqlAgentCreateFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparqlAgentCreateFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
