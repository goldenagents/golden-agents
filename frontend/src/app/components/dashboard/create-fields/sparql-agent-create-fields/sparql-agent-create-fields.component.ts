import { CrudAgent } from '../../../../models/crudagent';
import { DummyAgent } from '../../../../models/dummyagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sparql-agent-create-fields',
  templateUrl: './sparql-agent-create-fields.component.html',
  styleUrls: ['./sparql-agent-create-fields.component.css']
})
export class SparqlAgentCreateFieldsComponent implements OnInit {

  agent: DummyAgent;

  constructor(private service: AgentService, private ngbActiveModal: NgbActiveModal) { }

   ngOnInit() {
     const emptyMap: { [name: string]: string } =  {};
     emptyMap['icon'] = 'dns';
     this.agent = new DummyAgent('DB', 'DB', emptyMap);
   }

  onSubmit() {
    this.createAgent(this.agent);
    this.closeModal('Create Click');
  }

  private createAgent(dummy: DummyAgent) {
    this.service.createAgent(CrudAgent.fromDummy(dummy));
  }

  closeModal(reason: string) {
    this.ngbActiveModal.close(reason);
  }

}
