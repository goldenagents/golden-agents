import { CrudAgent } from '../../../../models/crudagent';
import { DummyAgent } from '../../../../models/dummyagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-broker-agent-create-fields',
  templateUrl: './broker-agent-create-fields.component.html',
  styleUrls: ['./broker-agent-create-fields.component.css']
})
export class BrokerAgentCreateFieldsComponent implements OnInit {

  agent: DummyAgent;

  constructor(private service: AgentService, private ngbActiveModal: NgbActiveModal) { }

   ngOnInit() {
     const emptyMap: { [name: string]: string } =  {};
     emptyMap['icon'] = 'call_split';
     this.agent = new DummyAgent('Broker', 'Broker', emptyMap);
   }

  onSubmit() {
    this.createAgent(this.agent);
    this.closeModal('Create Click');
  }

  private createAgent(dummy: DummyAgent) {
    this.service.createAgent(CrudAgent.fromDummy(dummy));
  }

  closeModal(reason: string) {
    this.ngbActiveModal.close(reason);
  }

}
