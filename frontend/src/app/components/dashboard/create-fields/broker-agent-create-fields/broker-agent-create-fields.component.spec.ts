import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerAgentCreateFieldsComponent } from './broker-agent-create-fields.component';

describe('BrokerAgentCreateFieldsComponent', () => {
  let component: BrokerAgentCreateFieldsComponent;
  let fixture: ComponentFixture<BrokerAgentCreateFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerAgentCreateFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerAgentCreateFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
