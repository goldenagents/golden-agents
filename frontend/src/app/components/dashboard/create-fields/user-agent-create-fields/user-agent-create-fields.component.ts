import { CrudAgent } from '../../../../models/crudagent';
import { DummyAgent } from '../../../../models/dummyagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-agent-create-fields',
  templateUrl: './user-agent-create-fields.component.html',
  styleUrls: ['./user-agent-create-fields.component.css']
})
export class UserAgentCreateFieldsComponent implements OnInit {

  agent: DummyAgent;

  constructor(private service: AgentService, private ngbActiveModal: NgbActiveModal) { }

   ngOnInit() {
     const emptyMap: { [name: string]: string } =  {};
     emptyMap['icon'] = 'person';
     this.agent = new DummyAgent('User', 'User', emptyMap);
   }

  onSubmit() {
    this.createAgent(this.agent);
    this.closeModal('Create Click');
  }

  private createAgent(dummy: DummyAgent) {
    this.service.createAgent(CrudAgent.fromDummy(dummy));
  }

  closeModal(reason: string) {
    this.ngbActiveModal.close(reason);
  }

}
