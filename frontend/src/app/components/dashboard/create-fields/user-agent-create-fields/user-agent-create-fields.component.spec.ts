import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAgentCreateFieldsComponent } from './user-agent-create-fields.component';

describe('UserAgentCreateFieldsComponent', () => {
  let component: UserAgentCreateFieldsComponent;
  let fixture: ComponentFixture<UserAgentCreateFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAgentCreateFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAgentCreateFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
