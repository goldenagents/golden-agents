import {CrudAgent} from '../../../models/crudagent';
import {SseService} from '../../../services/sse.service';
import {Component, ElementRef, Input, OnChanges, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {DbResultsetProgress, IAgentHash} from '../../../models/dbResultsetProgress';
import {QueryProgress} from '../../../models/queryprogress';
import {QueryProgressSubResult} from '../../../models/queryProgressSubResult';
import {QueryParseException} from '../../../models/queryParseException';

@Component({
  selector: 'app-query-info',
  templateUrl: './query-info.component.html',
  styleUrls: ['./query-info.component.css']
})

export class QueryInfoComponent implements OnChanges {
  @ViewChildren('query_progress') progressList: QueryList<ElementRef>;

  @Input() agent: CrudAgent;
  @Input() activeQueryID: string;

  public errorObject = {} as QueryParseException;
  dbAgentList: string[];
  dbAgentMap: IAgentHash;

  constructor(private sse: SseService) { }

  ngOnChanges(changes: SimpleChanges) {
    // const conversationID = changes['conversationID'].currentValue;
    if (changes['activeQueryID']) {
      const queryID = changes['activeQueryID'].currentValue;
      this.resetProgress(queryID != null);

      if (queryID != null) {
        // Subscribe to progress updates for this conversation
        this.sse.queryProgress(queryID).subscribe(progress => {
          this.updateProgress(progress.index, progress.value);
          if (progress.type === 'SUBQUERY_SENT') {
            this.updateDbAgentList(progress);
          } else if (progress.type === 'DATA_COLLECTED') {
            this.updateDbProgress(progress);
          } else if (progress.type === 'RESULTS_COLLECTED') {
            this.markProgressComplete();
          } else if (progress.type === 'DATABASE_ERROR') {
            this.setDatabaseError(progress);
          }
        });
      }

      this.sse.newMessages(this.agent).subscribe(newMessage => {
        if (newMessage.performative === 'NOT_UNDERSTOOD' || newMessage.header === 'QUERY_SYNTAX_ERROR') {
          const content = JSON.parse(newMessage.content);
          if (content.queryID !== null && content.queryID === this.activeQueryID) {
            this.errorObject = QueryParseException.fromEventData(JSON.parse(newMessage.content));
            this.setError(0, false, false);
          }
        }
      });
    }
  }

  private updateDbAgentList(progress: QueryProgress): void {
    if (progress.subresults === undefined) {
      return;
    }
    progress.subresults.forEach((value: QueryProgressSubResult, index: number) => {
      this.addDbAgent(value);
    });
  }

  private addDbAgent(agentInfo: QueryProgressSubResult): void {
    if (agentInfo !== null && agentInfo.stringvalue !== undefined && !(this.dbAgentList.includes(agentInfo.stringvalue))) {
      this.dbAgentList.push(agentInfo.stringvalue);
      this.dbAgentMap[agentInfo.stringvalue] = new DbResultsetProgress(agentInfo.stringvalue);
    }
  }

  private updateDbProgress(progress: QueryProgress): void {
    if (progress.subresults === undefined) {
      return;
    }
    progress.subresults.forEach((value: QueryProgressSubResult) => {
      if (value.stringvalue !== undefined) {
        this.addDbAgent(value);
        this.dbAgentMap[value.stringvalue].results += value.longvalue;
        this.dbAgentMap[value.stringvalue].setFinished(value.finished);
      }
    });
  }

  /**
   * Resets all progress markers
   * @param startNewProgress Boolean. Sets the first progress marker to processing iff true
   */
  private resetProgress(startNewProgress: boolean) {
    this.dbAgentList = [];
    this.dbAgentMap = {};
    this.errorObject = {} as QueryParseException;
    if (this.progressList !== undefined) {
      this.progressList.forEach((item: ElementRef, index: number) => {
        const elem: HTMLLIElement = item.nativeElement;
        elem.classList.remove('in-progress', 'progress-done', 'progress-error');
      });
      if (startNewProgress && this.activeQueryID.match(/[a-zA-Z]/)) {
        (this.progressList.first.nativeElement as HTMLLIElement).classList.add('in-progress');
      }
    }
  }

  /**
   * Mark a specific item in the progress list as failed.
   * @param index             Index of item in the progress list
   * @param beforeAsSuccess   If true, items before index will be marked as success, else, items will be unmarked
   * @param afterAsSuccess     If true, items after index will be marked as success, else, items will be unmarked
   */
  private setError(index: number, beforeAsSuccess: boolean, afterAsSuccess: boolean) {
    let foundIndex = false;
    this.progressList.forEach((item: ElementRef, i: number) => {
      const element: HTMLLIElement = item.nativeElement;
      if (i === index) {
        element.classList.remove('in-progress', 'progress-done');
        element.classList.add('progress-error');
        foundIndex = true;
      } else if (foundIndex) {
        element.classList.remove('in-progress');
        if (afterAsSuccess) {
          element.classList.add('progress-done');
        } else {
          element.classList.remove('progress-done');
        }
      } else {
        element.classList.remove('in-progress');
        if (beforeAsSuccess) {
          element.classList.add('progress-done');
        } else {
          element.classList.remove('progress-done');
        }
      }
    });
  }

  /**
   * Show all items in the progress list as done
   */
  private markProgressComplete() {
    if (this.progressList !== undefined) {
      this.progressList.forEach((item: ElementRef, index: number) => {
        const elem: HTMLLIElement = item.nativeElement;
        elem.classList.remove('in-progress');
        elem.classList.add('progress-done');
      });
    }
  }

  /**
   * Update the progress list with the latest progress update
   * @param index Index of the item in the progress list
   * @param value (Optional) extra value, that can be used to enhance the progress
   *              list item
   */
  private updateProgress(index: number, value: number) {
    this.progressList.forEach((element: ElementRef, i: number) => {
      const item: HTMLLIElement = element.nativeElement;
      if (i < index) {
        // Items before this index are now finished
        item.classList.remove('in-progress');
        item.classList.add('progress-done');
      } else if (i === index) {
        // The progress item we received an update for

        if (value !== null && value > 1) {
          // If we have a value larger than 1, this type takes some time. Assume it to be in progress
          item.classList.add('in-progress');
          const valueHolder = item.querySelector('.value-holder');
          if (valueHolder != null) {
            valueHolder.innerHTML = value.toString() + ' ';
          }
        } else {
          // Otherwise, mark this task as done
          item.classList.remove('in-progress');
          item.classList.add('progress-done');
        }
      } else if (i === index + 1 && (value === null || value <= 1)) {
        // If the current task is finished, mark the following task as being in progress
        item.classList.add('in-progress');
      }
    });
  }

  /**
   * Mark a data source as failed
   * @param progress Submitted SSE event notifying of database rror
   */
  private setDatabaseError(progress: QueryProgress): void {
    if (progress.subresults !== undefined) {
      this.dbAgentMap[progress.subresults[0].stringvalue].setError(progress.value);
    }
  }
}
