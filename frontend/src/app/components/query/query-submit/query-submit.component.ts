import {CrudAgent} from '../../../models/crudagent';
import {CrudMessage} from '../../../models/crudmessage';
import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {SseService} from '../../../services/sse.service';
import {UseragentService} from '../../../services/useragent.service';
import {QueryParseException} from '../../../models/queryParseException';
import {CachedQueryResult} from '../../../models/cachedQueryResult';
import {AgentService} from '../../../services/agent.service';
import {TextMarker} from 'codemirror';
import {QueryCommunicationService} from '../../../services/query-communication.service';

@Component({
  selector: 'app-query-submit',
  templateUrl: './query-submit.component.html',
  styleUrls: ['./query-submit.component.css']
})
export class QuerySubmitComponent implements OnInit, OnChanges {

  @Input() agent: CrudAgent;
  @Input() activeQueryID: string;
  @Input() activeResult: CachedQueryResult;
  @Input() selectedSources: [string];
  @Output() querySubmitted: EventEmitter<string> = new EventEmitter();
  @Output() historicalQuerySelected: EventEmitter<number> = new EventEmitter();

  public loading = false;

  public errorObject = {} as QueryParseException;
  private textMarkerObject: TextMarker = null;

  _tmpQuery;

  @ViewChild('errormessage', {static: false}) errormessage: ElementRef;
  @ViewChild('querycontent', {static: false}) querycontent;
  @ViewChild('askQueryForm', {static: false}) queryContentWrapper: ElementRef;

  protected placeholder = 'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n' +
    'PREFIX ga: <https://data.goldenagents.org/ontology/>\n\n' +
    'SELECT DISTINCT * WHERE ' +
    '{\n' +
    '\t?creativeAgent rdf:type ga:CreativeAgent .\n' +
    '\t?creativeAgent ga:hasName ?nameOfTheAgent\n' +
    '}';
  query = this.placeholder;

  constructor(
    private userAgentService: UseragentService,
    private sse: SseService,
    private agentService: AgentService,
    private queryService: QueryCommunicationService
  ) {}

  ngOnInit() {
    this.sse.newMessages(this.agent).subscribe(newMessage => {
      if (newMessage.performative === 'NOT_UNDERSTOOD') {
        const content = JSON.parse(newMessage.content);
        if (content.queryID != null && content.queryID === this.activeQueryID) {
          this.alertQuerySyntaxError(newMessage, true);
        }
      }
    });
    this.queryService.displayQueryRequested.subscribe(x => {
      this.displaySparqlQuery(x);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['activeResult']) {
      // Reset errors
      this.errorObject = {} as QueryParseException;

      const aq: CachedQueryResult = changes['activeResult'].currentValue;
      this.activeQueryID = aq.queryID;
      this.query = aq.query;
    } else if (changes['activeQueryID']) {
      // Reset errors
      this.errorObject = {} as QueryParseException;
    }
  }

  public displaySparqlQuery(query: string): void {
    console.log('Showing query ' + query);
    this._tmpQuery = this.query;
    this.query = query;
    this.errorObject = {} as QueryParseException;
    console.log('Displaying new query: ' + query);
    this.queryContentWrapper.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }

  private alertQuerySyntaxError(newMessage: CrudMessage, error: boolean): void {
    try {
      const exceptionInfo = QueryParseException.fromEventData(JSON.parse(newMessage.content));
      this.showError(exceptionInfo);
    } catch (e) {
      // For some reason, JSON parsing succeeds but still throws an error. Catch error here
    }
  }

  private showError(error: QueryParseException) {
    this.errorObject = error;
    this.findTextLocation(error);
  }

  private findTextLocation(error: QueryParseException) {
    console.log(error);
    const start = {line : error.line - 1, ch: error.column - 1};
    const end = this.querycontent.codeMirror.posFromIndex(this.querycontent.codeMirror.indexFromPos(start) + error.nCharacters);
    console.log(start, end);
    this.textMarkerObject = this.querycontent.codeMirror.markText(start,
      end, {inclusiveLeft: false, inclusiveRight: true, className: 'is-invalid', selectLeft: true, selectRight: true});
  }

  private intelligentSearch() {
    this.userAgentService.sendQuery(this.agent.uuid, this.query, this.selectedSources,
        'USER_INTELLIGENT_SEARCH').subscribe(queryRequestBody => {
      this.querySubmitted.emit(queryRequestBody.queryID);
    });
  }

  onSubmit() {
    this.errorObject = {} as QueryParseException;
    if (this.textMarkerObject != null) {
      this.textMarkerObject.clear();
      this.textMarkerObject = null;
    }
    this.loading = true;
    this.userAgentService.sendQuery(
      this.agent.uuid, this.query,
      this.selectedSources,
      'USER_QUERY'
    ).subscribe(queryRequestBody => {
      this.querySubmitted.emit(queryRequestBody.queryID);
      this.loading = false;
    });
  }

  public onSelect() {

  }
}

