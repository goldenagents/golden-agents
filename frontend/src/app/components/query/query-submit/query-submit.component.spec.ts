import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuerySubmitComponent } from './query-submit.component';

describe('QuerySubmitComponent', () => {
  let component: QuerySubmitComponent;
  let fixture: ComponentFixture<QuerySubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuerySubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuerySubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
