import {CrudAgent} from '../../../models/crudagent';
import {Component, EmbeddedViewRef, Input, OnChanges, SimpleChanges, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import 'datatables.net';
import 'datatables.net-buttons';
import 'jszip';
import 'datatables.net-buttons/js/buttons.colVis.min.js';
import 'datatables.net-buttons/js/buttons.flash.min.js';
import 'datatables.net-buttons/js/buttons.html5.min.js';
import 'datatables.net-buttons/js/buttons.print.min.js';
import {CachedQueryResult} from '../../../models/cachedQueryResult';

@Component({
  selector: 'app-query-result',
  templateUrl: './query-result.component.html',
  styleUrls: ['./query-result.component.css']
})
export class QueryResultComponent implements OnChanges {

  @ViewChild('tpl', {read: TemplateRef}) tpl: TemplateRef<any>;
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  @Input() agent: CrudAgent;
  @Input() activeResult: CachedQueryResult;
  headers: string = null;
  queryID: string = null;

  constructor() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['activeResult']) {
      const result: CachedQueryResult = changes['activeResult'].currentValue;
      if (result != null) {
        this.container.clear();
        this.headers = result.headers;
        this.queryID = result.queryID;
        this.container.insert(this.tpl.createEmbeddedView(null));
      }
    }
  }



}
