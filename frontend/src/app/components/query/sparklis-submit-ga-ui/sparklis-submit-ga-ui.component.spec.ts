import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparklisSubmitGaUiComponent } from './sparklis-submit-ga-ui.component';

describe('SparklisSubmitComponent', () => {
  let component: SparklisSubmitGaUiComponent;
  let fixture: ComponentFixture<SparklisSubmitGaUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparklisSubmitGaUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparklisSubmitGaUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
