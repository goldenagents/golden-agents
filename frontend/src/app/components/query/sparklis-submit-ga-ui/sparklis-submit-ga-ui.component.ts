import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {CrudAgent} from '../../../models/crudagent';
import {CachedQueryResult} from '../../../models/cachedQueryResult';
import {AQLQuery} from '../../../models/aql/AQLQuery';
import {AQLTypeItem} from '../../../models/aql/AQLTypeItem';
import {UseragentService} from '../../../services/useragent.service';
import {SseService} from '../../../services/sse.service';
import {AQLSuggestions} from '../../../models/aql/AQLSuggestions';
import {SelectableCrudAgent} from '../../agent/user/user-agent-container/user-agent-container.component';
import {QueryCommunicationService} from '../../../services/query-communication.service';
import {AQLIdentityItem} from '../../../models/aql/AQLIdentityItem';

@Component({
  selector: 'app-sparklis-submit-ga-ui',
  templateUrl: './sparklis-submit-ga-ui.component.html',
  styleUrls: ['./sparklis-submit-ga-ui.component.css']
})

export class SparklisSubmitGaUiComponent implements OnInit, OnChanges {
  @Input() agent: CrudAgent;
  @Input() agents: SelectableCrudAgent[];
  @Input() queryID: string;
  @Input() queryHistory: CachedQueryResult[];
  @Output() querySent: EventEmitter<string> = new EventEmitter();
  @Output() historicalQuerySelected: EventEmitter<number> = new EventEmitter();

  @ViewChildren('classpropexpendable') collapsibles: QueryList<ElementRef>;

  public aqlQuery: AQLQuery;
  public suggestions: AQLSuggestions = new AQLSuggestions([], [], [], -1, null, false);
  public ApiSuggestions: AQLSuggestions = new AQLSuggestions([], [], [], -1, null, false);

  public reversed = false;
  public filter = '';

  public showHelpMessage = false;
  public showMore = false;
  public showClasses = false;
  public showForwardProperties = false;
  public showBackwardProperties = false;
  public showInstances = false;
  public showOrder = false;
  public showSuggestions = false;

  public testQuery = [
    {
      'indentation' : 0,
      'elements' : [
        {
          'type' : 'filler',
          'name' : -1,
          'label' : 'Give me',
          'prefix' : '',
          'focus' : false
        },
        {
          'type': 'class',
          'name' : 0,
          'label': 'Author',
          'prefix': 'every',
          'focus': false
        }
      ]
    },
    {
      'indentation' : 1,
      'elements' : [
        {
          'type': 'backward',
          'name' : 1,
          'label': 'hasName',
          'prefix': 'that has a',
          'focus': false
        }
      ]
    },
    {
      'indentation' : 1,
      'elements' : [
        {
          'type' : 'filler',
          'name' : -1,
          'prefix' : '',
          'label' : 'and',
          'focus' : false
        },
        {
          'type' : 'backward',
          'name' : 2,
          'prefix' : 'that has a',
          'label' : 'authorOf',
          'focus' : false
        },
        {
          'type' : 'filler',
          'name' : -1,
          'prefix' : '',
          'label' : 'something',
          'focus' : false
        },
        {
          'type' : 'class',
          'name' : 3,
          'prefix': 'that is a',
          'label' : 'Book',
          'focus' : true
        },
        {
          'type' : 'backward',
          'name' : 4,
          'prefix' : 'that has a',
          'label' : 'hasPublished',
          'focus' : false
        }
      ]
    }
  ];

  public elementClicked(element: string, queryElement?: any): void { // TODO, change queryElement to proper type when created
    if (queryElement === undefined || queryElement.focus) {
      if (element === 'showMore') {
        const showMore = !(this.showMore || this.showSuggestions);
        this.hideAllExtraElements();
        this.showMore = showMore;
      } else if (element === 'classes') {
        this.hideAllExtraElements();
        this.showClasses = true;
        this.updateShowSuggestions();
      } else if (element === 'forwardProperties') {
        this.hideAllExtraElements();
        this.showForwardProperties = true;
        this.updateShowSuggestions();
      } else if (element === 'backwardProperties') {
        this.hideAllExtraElements();
        this.showBackwardProperties = true;
        this.updateShowSuggestions();
      } else if (element === 'union') {
        this.hideAllExtraElements();
        this.useragentService.addAQLUnion(this.agent.uuid).subscribe(this.queryChangedCallback);
      } else if (element === 'instances') {
        this.hideAllExtraElements();
        this.showInstances = true;
        this.updateShowSuggestions();
      } else if (element === 'order') {
        this.hideAllExtraElements();
        this.showOrder = true;
        this.updateShowSuggestions();
      } else if (element === 'remove') {
        this.hideAllExtraElements();
        this.removeFocus(this.aqlQuery.virtualFocus);
      }
    } else if (queryElement?.name !== -1) {
      console.log('Changing focus to ' + queryElement.name);
      this.useragentService.changeAQLFocus(this.agent.uuid, queryElement.focusTarget).subscribe(this.queryChangedCallback);
    }
  }

  private hideAllExtraElements(): void {
    this.filter = '';
    this.constructSuggestionsFromAPI();
    this.showMore = false;
    this.showClasses = false;
    this.showForwardProperties = false;
    this.showBackwardProperties = false;
    this.showInstances = false;
    this.showOrder = false;
    this.showSuggestions = false;
  }

  private updateShowSuggestions(): void {
    this.showSuggestions = this.showClasses || this.showForwardProperties || this.showBackwardProperties || this.showInstances || this.showOrder;
  }

  constructor(public useragentService: UseragentService, public sse: SseService, public queryCommunication: QueryCommunicationService) { }

  ngOnChanges(changes: SimpleChanges) {
    // TODO how to use updated query
    // console.log(changes);
    // if (changes['query'] !== undefined && changes['query'].currentValue !== undefined && this.ApiSuggestions.focus !== this.query.name) {
    //   console.log('Query changed!');
    //   console.log(changes['query'].currentValue);
    //   this.useragentService.getQuerySuggestions(this.agent.uuid).subscribe(suggestions => {
    //     this.ApiSuggestions = suggestions;
    //     console.log(suggestions);
    //     this.constructSuggestionsFromAPI();
    //   });
    // } else {
    //   console.log('Got changes, but no query :(');
    // }
  }

  queryChangedCallback = (data) => {
    this.aqlQuery = AQLQuery.fromData(data);
    console.log({query: this.aqlQuery});
    this.querySent.emit(this.aqlQuery.queryID);
  }

  ngOnInit() {
    this.aqlQuery = new AQLQuery([], undefined, undefined, undefined, null, false);
    this.useragentService.getAQLQueryJson(this.agent.uuid).subscribe(this.queryChangedCallback);
    this.sse.querySuggestions(this.agent).subscribe(e => {
      if (e.queryID === parseInt(this.queryID, 10)) {
        console.log(`Suggestions match! ${e.classes.length} classes, ${e.backwardProperties} backward crossing properties and ${e.forwardProperties.length} forward crossing properties`);
        this.ApiSuggestions = e;
        this.constructSuggestionsFromAPI();
      } else {
        console.log(`Got suggestions for query ${e.queryID}, was expecting for ${this.queryID}. Not updating`);
      }
    });
  }

  private updateSuggestions(): void {
    this.useragentService.getQuerySuggestions(this.agent.uuid).subscribe(suggestions => {
      this.ApiSuggestions = suggestions;
      console.log(suggestions);
      this.constructSuggestionsFromAPI();
    });
  }

  public constructSuggestionsFromAPI(): void {
    console.log('Constructing suggestions from API');
    this.suggestions = this.ApiSuggestions;
    if (this.reversed) {
      this.reverse();
    }
    if (this.filter !== '') {
      this.filterByMatch();
    }
  }

  private reverse(): void {
    let reversedTypes: AQLTypeItem[] = [];
    let reversedProperties: AQLTypeItem[] = [];
    this.suggestions.classes.forEach(c => {
      reversedTypes = reversedTypes.concat(this.getReversedItems(c, null));
    });
    this.suggestions.forwardProperties.concat(this.suggestions.backwardProperties).forEach(p => {
      reversedProperties = reversedProperties.concat(this.getReversedItems(p, null));
    });
    this.suggestions = new AQLSuggestions(
      reversedTypes,
      reversedProperties,
      this.suggestions.instances,
      this.suggestions.queryID,
      this.suggestions.focus,
      this.suggestions.finished
    );
  }

  private filterByMatch(): void {
      const filter = this.filter.toLowerCase();
      let filteredTypes: AQLTypeItem[] = [];
      let filteredProperties: AQLTypeItem[] = [];
    const filteredInstances = this.suggestions.instances.filter(i => i.label.toLowerCase().indexOf(filter) >= 0);
    this.suggestions.classes.forEach(c => {
        filteredTypes = filteredTypes.concat(this.getMatchingTypeItems(c, filter));
      });
      this.suggestions.forwardProperties.concat(this.suggestions.backwardProperties).forEach(p => {
        filteredProperties = filteredProperties.concat(this.getMatchingTypeItems(p, filter));
      });

      this.suggestions = new AQLSuggestions(
        filteredTypes,
        filteredProperties,
        filteredInstances,
        this.suggestions.queryID,
        this.suggestions.focus,
        this.suggestions.finished,
        this.suggestions.selected
      );
  }

  private getReversedItems(item: AQLTypeItem, accumulator: AQLTypeItem): AQLTypeItem[] {
    const newItem = new AQLTypeItem(item.label, item.type,
      accumulator == null ? [] : [accumulator],
      item.url, item.quantifier, item.forwardjump);
    if (item.subitems.length === 0) {
      return [newItem];
    } else {
      let reversedItems: AQLTypeItem[] = [];
      item.subitems.forEach(subitem => {
        reversedItems = reversedItems.concat(this.getReversedItems(subitem, newItem));
      });
      return reversedItems;
    }
  }

  private getMatchingTypeItems(item: AQLTypeItem, filter: string): AQLTypeItem[] {
    let matchingTypeItems: AQLTypeItem[] = [];
    let matchingSubItems: AQLTypeItem[] = [];

    for (let i = 0; i < item.subitems.length; i++) {
      matchingSubItems = matchingSubItems.concat(this.getMatchingTypeItems(item.subitems[i], filter));
    }

    if (this.typeItemMatches(item, filter)) {
      matchingTypeItems.push(new AQLTypeItem(
        item.label, item.type, matchingSubItems, item.url, item.quantifier, item.forwardjump
      ));
    } else {
      matchingTypeItems = matchingSubItems;
    }

    return matchingTypeItems;
  }

  private typeItemMatches(item: AQLTypeItem, filter: string) {
    return item.label.toLowerCase().indexOf(filter) >= 0 || item.url.toLowerCase().indexOf(filter) >= 0;
  }

  conceptSelected(item: AQLTypeItem): void {
    this.hideAllExtraElements();
    this.ApiSuggestions = new AQLSuggestions([], [], [], parseInt(this.aqlQuery.queryID, 10), null, false);
    this.constructSuggestionsFromAPI();
    if (item.type === 'class') {
      this.useragentService.addAQLClass(this.agent.uuid, item).subscribe(this.queryChangedCallback);
    } else if (item.type === 'instance') {
      this.useragentService.addAQLEntity(this.agent.uuid, item).subscribe(this.queryChangedCallback);
    } else {
      this.useragentService.addAQLProperty(this.agent.uuid, item).subscribe(this.queryChangedCallback);
    }

  }

  public removeFocus(focus: string): void {
    this.useragentService.removeAtFocus(this.agent.uuid, focus).subscribe(this.queryChangedCallback);
  }

  expandAll(expand: boolean): void {
    this.collapsibles.forEach(r => {
      r.nativeElement.checked = expand;
    });
  }
}
