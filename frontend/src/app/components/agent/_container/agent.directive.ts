// tslint:disable: directive-selector
import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[agentContainer]',
})
export class AgentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
