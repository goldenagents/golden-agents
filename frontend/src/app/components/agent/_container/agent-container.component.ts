import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CrudAgent} from '../../../models/crudagent';
import {ActivatedRoute} from '@angular/router';
import {AgentComponentService} from '../../../services/agent-component.service';
import {Title} from '@angular/platform-browser';
import {Subscription} from 'rxjs';
import {AgentDirective} from './agent.directive';
import {AgentService} from '../../../services/agent.service';

@Component({
  selector: 'app-agent',
  templateUrl: './agent-container.component.html',
  styleUrls: ['./agent-container.component.css']
})
export class AgentContainerComponent implements OnInit, OnDestroy {

  private dataSub: Subscription;
  @ViewChild(AgentDirective, {static: true}) agentContainer: AgentDirective;

  constructor(
    private route: ActivatedRoute,
    private componentService: AgentComponentService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private titleService: Title) {
  }

  ngOnInit(): void {
    // Get the data provided by the resolver
    this.dataSub = this.route.data.subscribe(data => {
      const agent = data.agent as CrudAgent;
      window.localStorage.setItem(AgentService.UUID_KEY, agent.uuid);
      this.titleService.setTitle(agent.nickname);
      this.componentService.agentContainer = this.agentContainer;
      this.componentService.addDynamicAgentComponents(agent);
    });
  }

  ngOnDestroy(): void {
    this.dataSub.unsubscribe();
  }
}
