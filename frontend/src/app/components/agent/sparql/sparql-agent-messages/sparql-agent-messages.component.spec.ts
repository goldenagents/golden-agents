import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparqlAgentMessagesComponent } from './sparql-agent-messages.component';

describe('SparqlAgentMessagesComponent', () => {
  let component: SparqlAgentMessagesComponent;
  let fixture: ComponentFixture<SparqlAgentMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparqlAgentMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparqlAgentMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
