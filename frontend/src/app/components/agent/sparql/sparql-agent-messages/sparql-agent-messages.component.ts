import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sparql-agent-messages',
  templateUrl: './sparql-agent-messages.component.html',
  styleUrls: ['./sparql-agent-messages.component.css']
})
export class SparqlAgentMessagesComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor() {}

  ngOnInit(): void {}

}
