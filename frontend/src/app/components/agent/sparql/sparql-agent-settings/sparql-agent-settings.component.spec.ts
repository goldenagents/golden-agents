import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparqlAgentSettingsComponent } from './sparql-agent-settings.component';

describe('SparqlAgentSettingsComponent', () => {
  let component: SparqlAgentSettingsComponent;
  let fixture: ComponentFixture<SparqlAgentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparqlAgentSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparqlAgentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
