import { CrudAgent } from '../../../../models/crudagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sparql-agent-settings',
  templateUrl: './sparql-agent-settings.component.html',
  styleUrls: ['./sparql-agent-settings.component.css']
})
export class SparqlAgentSettingsComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor(private route: ActivatedRoute, private agentService: AgentService) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.agent = data.agent as CrudAgent;
    });
  }

  onEdit(): void {
    this.agentService.updateAgent(this.agent);
    console.log(this.agent);
  }
}
