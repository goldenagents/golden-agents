import { CrudAgent } from '../../../../models/crudagent';
import {Component, OnInit, Input, AfterContentInit, ElementRef, ViewChild, OnChanges, SimpleChanges, Output, EventEmitter, PipeTransform, Pipe, AfterViewInit} from '@angular/core';
import {ExpertiseModel} from '../../../../models/ExpertiseModel';
import {DbagentService} from '../../../../services/dbagent.service';
import * as d3 from 'd3';

@Component({
  selector: 'app-sparql-agent-statistics',
  templateUrl: './sparql-agent-statistics.component.html',
  styleUrls: ['./sparql-agent-statistics.component.css']
})

export class SparqlAgentStatisticsComponent implements OnChanges, AfterViewInit {
  private readonly colors;
  private PieChartLegend;
  private PieChartClasses;
  private PieChartProperties;
  private barchart;
  private currentFixedIndex = -1;

  @ViewChild('piechart', {static: false}) piechartContainer: ElementRef;
  @ViewChild('barchart', {static: false}) barchartContainer: ElementRef;
  @ViewChild('sparqlExample', {static: false}) sparqlExampleContainer: ElementRef;

  // Bound by layout
  public query = '';

  private chartData: ExpertiseModel[];
  private totalNClasses: number;
  private totalNProperties: number;

  @Input()
  public agent: CrudAgent;
  @Output()
  displaySparqlQueryRequested: EventEmitter<string> = new EventEmitter<string>();

  constructor(private dbAgentService: DbagentService) {
    this.colors = d3.scaleOrdinal(d3.schemeCategory10);
    console.log(this.query);
  }

  /**
   * Extract a human readable variable name from a GA label
   * @param label   GA label
   * @return string   Human readable variable name
   */
  private static extractVariableFromLabel(label: string): string {
    return '?' + label.substr(label.indexOf(':') + 1).toLowerCase();
  }

  ngAfterViewInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['agent']) {
      this.dbAgentService.getExpertise(this.agent.uuid).subscribe((model: ExpertiseModel[]) => {
        if (model != null && model.length > 0) {
          this.chartData = model.sort((a, b) => {
            if (a.class === b.class) {
              return a.count > b.count ? -1 : a.count === b.count ? 0 : 1;
            } else {
              return a.class ? -1 : 1;
            }
          });
          const labels = this.chartData.map(d => d.label);
          for (let i = 0; i < this.chartData.length; i++) {
            this.chartData[i].index = i;
            this.chartData[i].color = this.colors(i);
            this.chartData[i].combinations.sort((a, b) => {
              const aInd = labels.indexOf(a.label);
              const bInd = labels.indexOf(b.label);
              return aInd < bInd ? -1 : aInd === bInd ? 0 : 1;
            });
            for (let j = 0; j < this.chartData[i].combinations.length; j++) {
              this.chartData[i].combinations[j].index = j;
              this.chartData[i].combinations[j].color = this.colors[j];
            }
          }

          // Clear existing graphs if present
          d3.select(this.piechartContainer.nativeElement).selectAll('svg').remove();
          d3.select(this.barchartContainer.nativeElement).selectAll('svg').remove();

          const allClasses = this.chartData.filter(m => m.class);
          const allProperties = this.chartData.filter(m => !m.class);
          this.totalNClasses = allClasses.length === 0 ? 0 : allClasses.map(m => m.count).reduce((x, y) => x + y);
          this.totalNProperties = allProperties.length === 0 ? 0 : allProperties.map(m => m.count).reduce((x, y) => x + y);
          this.PieChartClasses = this.pieChart(this.piechartContainer, this.chartData.filter(d => d.class), 'Classes');
          this.PieChartLegend = this.pieLegend(this.piechartContainer);
          this.PieChartProperties = this.pieChart(this.piechartContainer, this.chartData.filter(d => !d.class), 'Properties');
          this.barchart = this.histoGram(this.barchartContainer, this.chartData);

          this.PieChartClasses.add(this.PieChartProperties);
          this.PieChartProperties.add(this.PieChartClasses);
          this.barchartContainer.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
        }
      });
    }
  }

  copyQuery(): void {
    if (this.query !== '') {
      this.displaySparqlQueryRequested.emit(this.query);
    }
  }

  /**
   * Create the statement list for the example query, based on the two selected
   * GA labels
   * @param pieIndex    Index of selected slice in pie chart (containing GA label)
   * @param chartIndex  Index of selected bar in bar chart (containing GA label)
   */
  private updateExampleQuery(pieIndex: number, chartIndex: number): void {
    const pieClass = this.chartData[pieIndex];
    const chartClass = this.chartData[chartIndex];

    const classes: string[] = [];
    const relations: string[] = [];

    if (pieClass.class) {
      classes.push(pieClass.label);
    } else {
      relations.push(pieClass.label);
    }

    if (chartClass.class) {
      classes.push(chartClass.label);
    } else {
      relations.push(chartClass.label);
    }

    let lastClassVariable = '?a';
    const querySelectors = [];

    const statementList = [];

    if (classes.length > 0) {
      lastClassVariable = SparqlAgentStatisticsComponent.extractVariableFromLabel(classes[0]);
      for (let i = 0; i < classes.length; i++) {
        if (querySelectors.indexOf(lastClassVariable) < 0) {
          querySelectors.push(lastClassVariable);
        }
        statementList.push(lastClassVariable + ' rdf:type ' + classes[i]);
      }
    } else {
      querySelectors.push(lastClassVariable);
    }

    if (relations.length > 0) {
      for (let i = 0; i < relations.length; i++) {
        const relationVariable = SparqlAgentStatisticsComponent.extractVariableFromLabel(relations[i]);
        statementList.push(lastClassVariable + ' ' + relations[i] + ' ' + relationVariable);
        if (querySelectors.indexOf(relationVariable) < 0) {
          querySelectors.push(relationVariable);
        }
      }
    }

    this.query = 'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n' +
      'PREFIX ga: <https://data.goldenagents.org/ontology/>\n\n' +
      'SELECT DISTINCT ' + querySelectors.join(' ') + ' WHERE {\n' +
      '\t' + statementList.join(' . \n\t') + '\n' +
      '}';
  }

  public histoGram(selector: ElementRef, data: ExpertiseModel[]) {
    const self = this;
    const histoDim = {t: 60, r: 0, b: 100, l: 100, w: -1, h: -1};
    histoDim.w = 800 - histoDim.l - histoDim.r;
    histoDim.h = 600 - histoDim.t - histoDim.b;

    // Create SVG for histogram chart
    const histogramSvg = d3.select(selector.nativeElement)
      .append('svg')
      .attr('width', histoDim.w + histoDim.l + histoDim.r)
      .attr('height', histoDim.h + histoDim.t + histoDim.b).append('g')
      .attr('transform', 'translate(' + histoDim.l + ',' + histoDim.t + ')');

    // Create function for x-axis mapping
    const x = d3.scaleBand().rangeRound([0, histoDim.w]).padding(.1)
      .domain(data.map(function(d) { return d.label; }));

    // Create function for y-axis mapping
    const y = d3.scaleLinear().range([histoDim.h, 0])
      .domain([0, d3.max(data, function(d) { return d.count; })]) ;

    // Create separate function for y-axis grid and labels
    const yAxis = d3.scaleLinear().range([histoDim.h, 0])
      .domain([0, 100]);

    // Add x-axis to the histogram SVG
    histogramSvg.append('g').attr('class', 'x axis')
      .attr('transform', 'translate(0,' + histoDim.h + ')')
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'rotate(-45),translate(-10, -5 )')
      .attr('text-anchor', 'end');

    histogramSvg.append('g').attr('class', 'y axis grid')
      .call(d3.axisLeft(yAxis).ticks(10)
        .tickFormat((v, i) => (i * 10) + '%').tickSize(-histoDim.w));

    // Create bars for histogram to contain rectangles and class labels
    const bars = histogramSvg.selectAll('.bar').data(data).enter()
      .append('g').attr('class', 'bar');

    // Create the rectangles themselves
    bars.append('rect')
      .attr('x', function(d) { return x(d.label); })
      .attr('y', function(d) { return y(d.count); })
      .attr('width', x.bandwidth())
      .attr('height', function(d) { return histoDim.h - y(d.count); })
      .attr('fill', (d) => d.color)
      .on('mouseover', mouseover)
      .on('mouseout', mouseout)
      .on('click', click);

    // Create class labels with rectangles
    bars.append('text').text(function(d) { return d3.format(',')(d.count); })
      .attr('x', function(d) { return x(d.label) + x.bandwidth() / 2; })
      .attr('y', function(d) { return y(d.count) - 5; })
      .attr('text-anchor', 'middle');

    let waitForClick = -1;

    function mouseover(d) {
      if (waitForClick < 0 && self.currentFixedIndex >= 0) {
        self.updateExampleQuery(self.currentFixedIndex, d.index);
      }
    }

    function mouseout() {
      if (waitForClick < 0) {
        self.query = '';
      }
    }

    function click(d, i) {
      if (waitForClick === d.index) {
        waitForClick = -1;
        histogramSvg.selectAll('.bar').attr('opacity', 1);
      } else {
        histogramSvg.selectAll('.bar').attr('opacity', function(dat, ind) {
          return ind === i ? 1 : .5;
        });
        waitForClick = -1;
        mouseover(d);
        waitForClick = d.index;
      }
    }

    const resetOpacity = function() {
      waitForClick = -1;
      histogramSvg.selectAll('.bar').attr('opacity', 1);
      self.query = '';
    };

    const update = function(index: number) {
      // Prepare only subsection of data to be visualized
      let newBarData: ExpertiseModel[];
      if (index >= 0 && index < self.chartData.length) {
        newBarData = self.chartData[index].combinations.map(d => ExpertiseModel.fromCombination(d, false));
        newBarData.forEach((d, i) => {d.index = i; d.color = self.colors(i); });
      } else {
        newBarData = self.chartData;
      }

      // Update the domain of the y-axis map to reflect the changed data
      y.domain([0, d3.max(newBarData, function(d) { return d.count; })]);

      // Attach the new data to the bars
      const newBars = histogramSvg.selectAll('.bar').data(newBarData);

      // Transition the height of the rectangles
      newBars.select('rect').transition().duration(500)
        .attr('y', function(d: any) { return y(d.count); })
        .attr('height', function(d: any) { return histoDim.h - y(d.count); })
        .attr('fill', (d) => d.color);

      // Transition the label location and change the values
      newBars.select('text').transition().duration(500)
        .text(function(d: any) { return d3.format(',')(d.count); })
        .attr('y',  function(d: any) { return y(d.count) - 5; });
    };

    return {update: update, resetOpacity: resetOpacity};
  }

  /**
   * Create the SVG where the legend can be shown for either of the pie charts
   * @param selector  Dashboard element, where pie chart should be drawn
   */
  public pieLegend(selector: ElementRef) {
    const self = this;

    const legendSvg = d3.select(selector.nativeElement)
      .append('svg').attr('width', 350).attr('height', 250)
      .append('g').attr('transform', 'translate(15,15)');

    const updateLegend = function(d: ExpertiseModel) {
      legendSvg.selectAll('*').remove();
      legendSvg.append('circle').attr('x', 20).attr('y', 20).attr('r', 10).attr('fill', d.color);
      legendSvg.append('text').attr('x', 20).text(d.label).attr('y', 5).attr('size', 'sm');

      const total = d.class ? self.totalNClasses : self.totalNProperties;
      const totalText = d.class ? '% of all class assignments' : '% of all predicate usage';
      const help = d.class ? ' individuals are of this type' : ' statements use this predicate';
      legendSvg.append('text').attr('y', 40).text(d.count + help);
      legendSvg.append('text').attr('y', 60).text((d.count / total * 100).toFixed(2) + totalText);
    };

    const clear = function() {
      legendSvg.selectAll('*').remove();
    };

    return {
      'update' : updateLegend,
      'clear' : clear
    };
  }

  /**
   * Create the pie chart containing the counts of all classes
   * @param selector  Dashboard element, where pie chart should be drawn
   * @param data      Data to be drawn
   * @param title     Title of the pie chart, to be rendered on screen
   */
  public pieChart(selector: ElementRef, data: ExpertiseModel[], title: string) {
    const self = this;
    const otherPieCharts = [];

    const pieDim = {w : 250, h : 250 , r : 125, titleSpace: 24, titleMargin: 5};
    pieDim.r = Math.min(pieDim.w, pieDim.h) / 2;

    // Create svg for pie chart
    const piesvg = d3.select(selector.nativeElement).append('svg')
      .attr('width', pieDim.w).attr('height', pieDim.h + pieDim.titleSpace + pieDim.titleMargin)
      .attr('fill', '#efefef')
      .append('g')
      .attr('transform', 'translate(' + pieDim.h / 2 + ', ' + (pieDim.h / 2 + pieDim.titleSpace + pieDim.titleMargin) + ')');

    piesvg.append('text')
      .attr('x', 0)
      .attr('y', -1 * pieDim.h / 2 - pieDim.titleMargin)
      .attr('text-anchor', 'middle')
      .style('font-size', pieDim.titleSpace + 'px')
      .style('font-weight', 'bold')
      .style('fill', '#000000')
      .text(title);

    // Create function to draw the arcs of the pie slices
    const arc = d3.arc<any>().outerRadius(pieDim.r - 10).innerRadius(0);

    // Create a different function to draw the one arc that is highlighted
    const arcHighlighted = d3.arc<any>().outerRadius(pieDim.r).innerRadius(0);

    // Create a function to compute the pie slice angles
    const pie = d3.pie<ExpertiseModel>().sort(null).value(function(d) {return d.count; });

    // Draw the pie slices
    piesvg.selectAll('path').data(pie(data)).enter().append('path')
      .attr('d', arc)
      .attr('class', 'arc')
      .each(function(d) { (this as any)._current = d; } )
      .style('fill', (d) => d.data.color)
      .on('mouseover', mouseover).on('mouseout', mouseout).on('click', clicked);

    // Utility function to be called on mouse over a pie slice
    function mouseover(d, i) {
      if (self.currentFixedIndex < 0 || self.currentFixedIndex === d.data.index) {
        self.barchart.update(d.data.index);
      }
      piesvg.selectAll('.arc').attr('d', function(dat, ind) {
        return (ind === i) ? arcHighlighted(dat) : arc(dat);
      });

      self.PieChartLegend.update(d.data);
    }

    // Utility function to be called on mouse out a pie slice
    function mouseout(d, i) {
      d3.select('#mouseover-label-' + i).remove();
      piesvg.selectAll('.arc').attr('d', arc);
      if (self.currentFixedIndex < 0) {
        self.barchart.update(-1);
      }
      self.PieChartLegend.clear();
    }

    function clicked(d, i) {
      if (self.currentFixedIndex === d.data.index) {
        // Remove filter
        self.currentFixedIndex = -1;
        piesvg.selectAll('.arc').attr('opacity', 1);
        otherPieCharts.forEach(o => o.setOpacity(1));
      } else {
        self.currentFixedIndex = d.data.index;
        piesvg.selectAll('.arc').attr('opacity', function(dat, ind) {
          return (ind === i) ? '1' : '.5';
        });
        otherPieCharts.forEach(o => o.setOpacity(.5));
      }
      self.barchart.resetOpacity();
      mouseover(d, i);
    }

    function setOpacity(opacity: number) {
      piesvg.selectAll('.arc').attr('opacity', opacity);
    }

    /**
     * Add a sibling pie chart, which shares the data of this pie chart
     * @param other   Sibling pie chart object
     */
    function addPieChart(other: any) {
      if (!(other in otherPieCharts)) {
        otherPieCharts.push(other);
      }
    }

    /**
     * Remove a sibling pie chart, which shares the data of this pie chart
     * @param other   Sibling pie chart object
     */
    function removePieChart(other: any) {
      const ind = otherPieCharts.indexOf(other);
      if (ind >= 0) {
        otherPieCharts.slice(ind, 1);
      }
    }

    return {update: function() {}, setOpacity: setOpacity, add: addPieChart, remove: removePieChart};
  }
}
