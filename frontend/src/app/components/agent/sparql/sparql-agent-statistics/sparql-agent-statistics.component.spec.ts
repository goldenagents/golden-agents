import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparqlAgentStatisticsComponent } from './sparql-agent-statistics.component';

describe('SparqlAgentStatisticsComponent', () => {
  let component: SparqlAgentStatisticsComponent;
  let fixture: ComponentFixture<SparqlAgentStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparqlAgentStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparqlAgentStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
