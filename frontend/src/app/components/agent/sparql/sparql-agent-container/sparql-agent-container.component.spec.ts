import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparqlAgentContainerComponent } from './sparql-agent-container.component';

describe('SparqlAgentContainerComponent', () => {
  let component: SparqlAgentContainerComponent;
  let fixture: ComponentFixture<SparqlAgentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparqlAgentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparqlAgentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
