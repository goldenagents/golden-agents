import {CrudAgent} from '../../../../models/crudagent';
import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {SseService} from '../../../../services/sse.service';
import {UseragentService} from '../../../../services/useragent.service';
import {CachedQueryResult} from '../../../../models/cachedQueryResult';
import {animate, style, transition, trigger} from '@angular/animations';
import {QueryParseException} from '../../../../models/queryParseException';
import {QueryCommunicationService} from '../../../../services/query-communication.service';

export interface SelectableCrudAgent {
  agent: CrudAgent;
  selected: boolean;
}

@Component({
  selector: 'app-user-agent-container',
  templateUrl: './user-agent-container.component.html',
  styleUrls: ['./user-agent-container.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({height: 0, opacity: 0}),
          animate('300ms', style({height: '*', opacity: 1}))
        ]),
        transition(':leave', [
          style({height: '*', opacity: 1}),
          animate('200ms', style({height: 0, opacity: 0}))
        ])
      ]
    )
  ]
})
export class UserAgentContainerComponent implements OnInit, OnDestroy {

  @Input() public agent: CrudAgent;

  private routeSub: Subscription;
  private dataSub: Subscription;
  private readyAgentSubscription: Subscription;

  platformAgents: SelectableCrudAgent[] = [];
  brokerAgent: CrudAgent;
  selectedSources: string[] = [];
  showAgentStatistics: CrudAgent = null;
  showStatisticsHelp = false;

  activeQueryID: string;
  queryHistory = [];
  activeResult: CachedQueryResult;
  selectedTab: number;

  public errorObject = {} as QueryParseException;
  public tab = 0;
  public splitInterfaceVal = false;
  public matchMedia = window.matchMedia('(max-width: 1300px)');
  public webowl_ontology_uri: string = null;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private userAgentService: UseragentService,
    private sseService: SseService,
    private queryService: QueryCommunicationService
  ) { }

  ngOnInit() {
    this.dataSub = this.route.data.subscribe(data => {
      this.agent = data.agent as CrudAgent;
      this.platformAgents = (data.agents as CrudAgent[]).filter(x => x.agentType === 'DB').map(x => ({agent: x, selected: true}));
      this.brokerAgent = (data.agents as CrudAgent[]).find(a => a.agentType === 'Broker');
      if (this.brokerAgent != null && this.brokerAgent.homepage.trim().length > 0 && this.brokerAgent.homepage != null) {
        console.log({broker: this.brokerAgent});
        const argument = this.brokerAgent.homepage.indexOf('default-graph-uri=') >= 0 ? '&' : '?';
        const virtualOntology = `${this.brokerAgent.homepage}${argument}query=${encodeURIComponent('CONSTRUCT {?x ?y ?z} WHERE {?x ?y ?z}')}`;
        const virtualOntologyEncoded = encodeURIComponent(virtualOntology);
        this.webowl_ontology_uri = 'https://service.tib.eu/webvowl/#iri=' + virtualOntologyEncoded;
      }
      this.dataSourcesChanged();
    });

    this.queryService.beforeDisplayQueryRequested.subscribe(_ => {
      this.tab = 1;
    });

    this.readyAgentSubscription = this.sseService.readyAgents.subscribe(readyAgentUUID => {
      const index = this.platformAgents.findIndex(agent => agent.agent.uuid === readyAgentUUID);
      if (index >= 0) {
        console.log(this.platformAgents[index].agent.nickname + ' ready to do some work');
        this.platformAgents[index].agent.ready = true;
      }
    });

    this.userAgentService.getQueryHistory(this.agent.uuid).subscribe(historyList => {
      this.queryHistory = historyList.map(item => CachedQueryResult.fromData(item));
      if (this.queryHistory.length >= 1) {
        this.activeResult = this.queryHistory[this.queryHistory.length - 1];
      }
    });

    this.sseService.newMessages(this.agent).subscribe(newMessage => {
      if (newMessage.performative === 'NOT_UNDERSTOOD' || newMessage.header === 'QUERY_SYNTAX_ERROR') {
        const content = JSON.parse(newMessage.content);
        if (content.queryID !== null && content.queryID === this.activeQueryID) {
          this.errorObject = QueryParseException.fromEventData(JSON.parse(newMessage.content));
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.dataSub.unsubscribe();
    this.readyAgentSubscription.unsubscribe();
  }

  querySentHandler(queryID: string) {
    this.selectedTab = 0;
    this.errorObject = {} as QueryParseException;
    this.activeQueryID = queryID;
    this.sseService.queryProgress(queryID).subscribe(update => {
      if (update.type === 'RESULTS_COLLECTED') {
        this.selectedTab = 1;
        this.activeResult = CachedQueryResult.fromData(update.value);
        this.queryHistory.push(this.activeResult);
      }
    });
  }

  historicalQuerySelected(index: number) {
    if (index >= 0 && index < this.queryHistory.length) {
      this.activeResult = this.queryHistory[index];
      if (this.activeResult.finished) {
        this.selectedTab = 1;
      }
    }
  }

  dataSourcesChanged(): void {
    this.selectedSources = this.platformAgents.filter(x => x.selected).map(x => x.agent.uuid);
  }
}
