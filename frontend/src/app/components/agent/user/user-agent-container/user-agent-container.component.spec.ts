import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAgentContainerComponent } from './user-agent-container.component';

describe('UserAgentContainerComponent', () => {
  let component: UserAgentContainerComponent;
  let fixture: ComponentFixture<UserAgentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAgentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAgentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
