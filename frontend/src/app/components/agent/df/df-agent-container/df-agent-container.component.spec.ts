import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DfAgentContainerComponent } from './df-agent-container.component';

describe('DfAgentContainerComponent', () => {
  let component: DfAgentContainerComponent;
  let fixture: ComponentFixture<DfAgentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfAgentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfAgentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
