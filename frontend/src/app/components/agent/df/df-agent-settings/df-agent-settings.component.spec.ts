import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DfAgentSettingsComponent } from './df-agent-settings.component';

describe('DfAgentSettingsComponent', () => {
  let component: DfAgentSettingsComponent;
  let fixture: ComponentFixture<DfAgentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfAgentSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfAgentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
