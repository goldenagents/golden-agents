import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-df-agent-messages',
  templateUrl: './df-agent-messages.component.html',
  styleUrls: ['./df-agent-messages.component.css']
})
export class DfAgentMessagesComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor() {}

  ngOnInit(): void {}

}
