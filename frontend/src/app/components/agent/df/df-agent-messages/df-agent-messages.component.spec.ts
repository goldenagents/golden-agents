import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DfAgentMessagesComponent } from './df-agent-messages.component';

describe('DfAgentMessagesComponent', () => {
  let component: DfAgentMessagesComponent;
  let fixture: ComponentFixture<DfAgentMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfAgentMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfAgentMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
