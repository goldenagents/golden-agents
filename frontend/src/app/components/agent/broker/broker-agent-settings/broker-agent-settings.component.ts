import { CrudAgent } from '../../../../models/crudagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-broker-agent-settings',
  templateUrl: './broker-agent-settings.component.html',
  styleUrls: ['./broker-agent-settings.component.css']
})
export class BrokerAgentSettingsComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor(private agentService: AgentService) {}

  ngOnInit(): void {}

  onEdit(): void {
    this.agentService.updateAgent(this.agent);
    console.log(this.agent);
  }
}

