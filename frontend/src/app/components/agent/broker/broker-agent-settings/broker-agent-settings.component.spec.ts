import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerAgentSettingsComponent } from './broker-agent-settings.component';

describe('BrokerAgentSettingsComponent', () => {
  let component: BrokerAgentSettingsComponent;
  let fixture: ComponentFixture<BrokerAgentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerAgentSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerAgentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
