import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-broker-agent-messages',
  templateUrl: './broker-agent-messages.component.html',
  styleUrls: ['./broker-agent-messages.component.css']
})
export class BrokerAgentMessagesComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor() {}

  ngOnInit(): void {}

}
