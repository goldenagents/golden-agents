import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerAgentMessagesComponent } from './broker-agent-messages.component';

describe('BrokerAgentMessagesComponent', () => {
  let component: BrokerAgentMessagesComponent;
  let fixture: ComponentFixture<BrokerAgentMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerAgentMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerAgentMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
