import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerAgentContainerComponent } from './broker-agent-container.component';

describe('BrokerAgentContainerComponent', () => {
  let component: BrokerAgentContainerComponent;
  let fixture: ComponentFixture<BrokerAgentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerAgentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerAgentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
