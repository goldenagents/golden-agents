import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-broker-agent-container',
  templateUrl: './broker-agent-container.component.html',
  styleUrls: ['./broker-agent-container.component.css']
})
export class BrokerAgentContainerComponent implements OnInit, OnDestroy {

  selectedTab: number;
  private routeSub: Subscription;
  private dataSub: Subscription;
  agent: CrudAgent;
  private tabs = new Map<string, number>();

  constructor(private route: ActivatedRoute) {
    this.tabs['messages'] = 1;
    this.tabs['settings'] = 2;
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      const tab = params['tab'];
      if (tab) {
        this.selectedTab = this.tabs[tab];
      }
    });
    this.dataSub = this.route.data.subscribe(data => {
      this.agent = data.agent as CrudAgent;
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.dataSub.unsubscribe();
  }
}

