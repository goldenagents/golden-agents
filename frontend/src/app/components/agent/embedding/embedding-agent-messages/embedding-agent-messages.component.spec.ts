import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentMessagesComponent } from './embedding-agent-messages.component';

describe('EmbeddingAgentMessagesComponent', () => {
  let component: EmbeddingAgentMessagesComponent;
  let fixture: ComponentFixture<EmbeddingAgentMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
