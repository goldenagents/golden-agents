import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-embedding-agent-messages',
  templateUrl: './embedding-agent-messages.component.html',
  styleUrls: ['./embedding-agent-messages.component.css']
})
export class EmbeddingAgentMessagesComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor() {}

  ngOnInit(): void {}
}
