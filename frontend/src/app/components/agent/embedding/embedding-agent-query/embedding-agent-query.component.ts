import {CrudAgent} from '../../../../models/crudagent';
import { EmbeddingResult } from '../../../../models/embeddingresult';
import { EmbeddingService } from '../../../../services/embedding.service';
import { RadialBarComponent } from '../../../misc/radial-bar/radial-bar.component';
import {Component, OnInit, Input, ElementRef, OnDestroy, ViewContainerRef, ViewChild} from '@angular/core';
// import {D3} from 'd3-ng2-service'; // TODO D3 service was removed

@Component({
  selector: 'app-embedding-agent-query',
  templateUrl: './embedding-agent-query.component.html',
  styleUrls: ['./embedding-agent-query.component.css']
})
export class EmbeddingAgentQueryComponent implements OnInit, OnDestroy {

  @Input()
  protected agent: CrudAgent;
  // private d3: D3; // TODO d3 service was removed
  private parentNativeElement: any;
  query: string;
  k: number;
  results: EmbeddingResult[];
  data1: number[];
  data2: number[];

  @ViewChild('embeddingQueryForm' , {static: false}) createFormContainer: ViewContainerRef;
  @ViewChild('radialBar', {static: false}) focusRadial: RadialBarComponent;

  constructor(
    private element: ElementRef,
    // private d3Service: D3Service, // TODO D3 service was removed
    private embedding: EmbeddingService) {

    // this.d3 = d3Service.getD3(); // TODO D3 service was removed
    this.parentNativeElement = element.nativeElement;
  }

  onSubmit(): void {
    this.embedding.getNearestNeighbours(this.agent, this.query, this.k).subscribe(result => {
      const results = result as EmbeddingResult[];
      this.results = results.sort((a, b) => a.distance > b.distance ? 1 : ((b.distance > a.distance) ? -1 : 0));
      this.focusRadial.setFocusData(results[0].vector);
    });
  }

  compareResult(result: EmbeddingResult): void {
    this.focusRadial.setCompareData(result.vector);
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }

}
