import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentQueryComponent } from './embedding-agent-query.component';

describe('EmbeddingAgentQueryComponent', () => {
  let component: EmbeddingAgentQueryComponent;
  let fixture: ComponentFixture<EmbeddingAgentQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
