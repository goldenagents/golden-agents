import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentInfoComponent } from './embedding-agent-info.component';

describe('EmbeddingAgentInfoComponent', () => {
  let component: EmbeddingAgentInfoComponent;
  let fixture: ComponentFixture<EmbeddingAgentInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
