import {CrudAgent} from '../../../../models/crudagent';
import {SseService} from '../../../../services/sse.service';
import {LineChartComponent} from '../../../misc/line-chart/line-chart.component';
import {ProgressBarComponent} from '../../../misc/progress-bar/progress-bar.component';
import {Component, OnInit, Input, OnDestroy, ViewChild, ViewContainerRef, AfterContentInit, AfterViewInit} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-embedding-agent-boot',
  templateUrl: './embedding-agent-boot.component.html',
  styleUrls: ['./embedding-agent-boot.component.css'],
})
export class EmbeddingAgentBootComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input()
  protected agent: CrudAgent;
  private progressSubscription: Subscription;
  gdMaxIter: number;
  @ViewChild('lineChart', {static: false}) gloveChart: LineChartComponent;
  @ViewChild('progressBar', {static: false}) bcaProgress: ProgressBarComponent;

  constructor(private sse: SseService) {}

  ngOnInit(): void {
    this.gdMaxIter = +this.agent.additionals['gd.max.iter'];
    this.progressSubscription = this.sse.embeddingProgress(this.agent).subscribe(progress => {
      if (progress.type.toLowerCase() === 'bca') {
        this.bcaProgress.update(progress.value);
        if (progress.finished) {
          this.bcaProgress.setFinished();
        }
      } else if (progress.type.toLowerCase() === 'glove') {
        this.bcaProgress.setFinished();
        this.gloveChart.update(progress.value);
        if (progress.finished) {
          this.gloveChart.setFinished();
        }
      }
    });
  }

  ngAfterViewInit(): void {
   // In this case the agent has already booted so we set everything to done and fill in the line chart
    if (this.agent.additionals['gd.cost.history']) {
      const history = JSON.parse(this.agent.additionals['gd.cost.history']) as number[];
      this.bcaProgress.setFinished();
      this.gloveChart.updateAll(history);
      this.gloveChart.setFinished();
    }
  }

  ngOnDestroy(): void {
    if (this.progressSubscription) {
      this.progressSubscription.unsubscribe();
    }
  }

}
