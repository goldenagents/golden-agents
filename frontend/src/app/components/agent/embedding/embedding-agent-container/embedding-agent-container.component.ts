import { CrudAgent } from '../../../../models/crudagent';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-embedding-agent-container',
  templateUrl: './embedding-agent-container.component.html',
  styleUrls: ['./embedding-agent-container.component.css']
})
export class EmbeddingAgentContainerComponent implements OnInit, OnDestroy  {

  selectedTab: number;
  private routeSub: Subscription;
  private dataSub: Subscription;
  agent: CrudAgent;
  private tabs = new Map<string, number>();

  constructor(private route: ActivatedRoute) {
    this.tabs['boot'] = 1;
    this.tabs['query'] = 2;
    this.tabs['messages'] = 3;
    this.tabs['settings'] = 4;
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      const tab = params['tab'];
      if (tab) {
        this.selectedTab = this.tabs[tab];
      }
    });
    this.dataSub = this.route.data.subscribe(data => {
      this.agent = data.agent as CrudAgent;
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.dataSub.unsubscribe();
  }
}
