import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentContainerComponent } from './embedding-agent-container.component';

describe('EmbeddingAgentContainerComponent', () => {
  let component: EmbeddingAgentContainerComponent;
  let fixture: ComponentFixture<EmbeddingAgentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
