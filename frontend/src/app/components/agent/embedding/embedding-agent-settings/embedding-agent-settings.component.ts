import { CrudAgent } from '../../../../models/crudagent';
import { AgentService } from '../../../../services/agent.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-embedding-agent-settings',
  templateUrl: './embedding-agent-settings.component.html',
  styleUrls: ['./embedding-agent-settings.component.css']
})
export class EmbeddingAgentSettingsComponent implements OnInit {

  @Input() agent: CrudAgent;

  constructor(private agentService: AgentService) {}

  ngOnInit(): void {}

  onEdit(): void {
    this.agentService.updateAgent(this.agent);
    console.log(this.agent);
  }

}
