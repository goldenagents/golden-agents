import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddingAgentSettingsComponent } from './embedding-agent-settings.component';

describe('EmbeddingAgentSettingsComponent', () => {
  let component: EmbeddingAgentSettingsComponent;
  let fixture: ComponentFixture<EmbeddingAgentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbeddingAgentSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddingAgentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
