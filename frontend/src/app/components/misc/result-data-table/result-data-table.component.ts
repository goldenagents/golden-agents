import {AfterContentInit, AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild} from '@angular/core';
import {CrudAgent} from '../../../models/crudagent';
import {CachedQueryResult} from '../../../models/cachedQueryResult';
import {environment as env} from '../../../../environments/environment';
import {UseragentService} from '../../../services/useragent.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-result-data-table',
  templateUrl: './result-data-table.component.html',
  styleUrls: ['./result-data-table.component.css']
})
export class ResultDataTableComponent implements OnDestroy, AfterViewInit {

  private dataTable: DataTables.DataTables;

  @ViewChild('result_table') resultTable: ElementRef;
  @ViewChild('result_head') resultHead: ElementRef;
  @Input() agent: CrudAgent;
  @Input() activeResult: CachedQueryResult;
  @Input() headers: string;
  @Input() queryID: string;

  constructor(private userAgentService: UseragentService) { }

  ngOnDestroy(): void {
    this.dataTable?.destroy(true);
  }

  ngAfterViewInit() {
    console.log('Trying to create table');
    const self = this;
    const head: any[] = JSON.parse(this.headers);

    if (head == null || head.length === 0) {
      return;
    }

    const tableHeaders = head.map(x => ({data: x.name, name: x.name, title: x.name}) );

    // Setup data tables
    // We cast the element to any, so we can use the JQuery data table element selector,
    // which does not have type definitions that prohibit the use of buttons
    const table: any = $(this.resultTable.nativeElement);

    this.dataTable = table.DataTable({
      'processing' : true,
      'serverSide' : true,
      'ajax' : {
        'url' : env.BASE_API_URL() + 'agent/user/queryresult',
        'contentType': 'application/x-www-form-urlencoded; charset=utf-8',
        'type' : 'POST',
        'data' : function(data) {
          return $.extend( {}, data, {
            'agentUUID' : self.agent.uuid,
            'queryID' : self.queryID
          });
        }
      },
      'columns' : tableHeaders,
      dom: '<"row"<"col-sm-12 col-md-6"B><"col-sm-12 col-md-6"f>>' +
        '<"row"<"col sm-12 col-md-3"l><"col-sm-12 col-md-3"i><"col sm-12 col-md-6"p>>' +
        '<"row"<"col-sm-12"tr>>' +
        '<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>',
      'paging': true,
      'searching': true,
      'info': true,
      'lengthChange': true,
      'lengthMenu': [10, 25, 50, 100],
      'pageLength': 10,
      'buttons' : [
        {
          extend: 'csv',
          title: 'results',
          className: 'btn btn-secondary',
          action: function(event, datatable, node, config) {
            self.openDownloadLink(self.userAgentService.getDownloadUrl(self.agent.uuid, self.queryID, 'csv'));
          }
        },
        {
          'text' : 'XML',
          className: 'btn btn-secondary',
          action: function(event, datatable, node, config) {
            self.openDownloadLink(self.userAgentService.getDownloadUrl(self.agent.uuid, self.queryID, 'xml'));
          }
        }
      ],
      'language' : {
        'paginate' : {
          'previous' : '← Previous',
          'next' : 'Next →'
        }
      }
    });

    // TODO this is not actually working. Use elemRef approach like below, but tricky, because we can't name the element we want to select. Gotta love angular!
    $('div.dt-buttons').addClass('btn-group');

    const tableHeaderElements = this.resultHead.nativeElement?.children?.[0]?.children;
    for (let i = 0; i < tableHeaderElements.length; i++) {
      tableHeaderElements[i].title = head[i].sources;
    }

    console.log('Table constructed');
  }

  private openDownloadLink(link: string): void {
    const a = document.createElement('A');
    a.setAttribute('href', link);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

}
