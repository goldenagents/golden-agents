import {Component, ViewEncapsulation, OnInit, Input} from '@angular/core';
import {ChartValue} from './chartvalue';
// import {D3Service, D3, ScaleLinear} from 'd3-ng2-service';  // TODO D3 service was removed
import {Line} from 'd3-shape';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  @Input()
  private xMax: number;
  private yMax: number;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  // TODO D3 service was removed
  // private x: ScaleLinear<number, number>;
  // private y: ScaleLinear<number, number>;
  private svg: any;
  private line: Line<[number, number]>;
  private data: number[][];
  // TODO D3 service was removed
  // private d3: D3;

  constructor(
    // TODO D3 service was removed
    // private d3Service: D3Service
  ) {
    // TODO D3 service was removed
    // this.d3 = d3Service.getD3();
    this.width = 1000 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.yMax = 0;
    this.data = [];
  }

  ngOnInit(): void {
    this.initSvg();
    this.initAxis();
    this.initLine();
    this.drawAxis();
    this.drawLine();
  }

  private initSvg(): void {
    // TODO D3 service was removed
    // this.svg = this.d3.select('svg#lineChart')
    //   .append('g')
    //   .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private initAxis(): void {
    // TODO D3 service was removed
    // this.x = this.d3.scaleLinear().range([0, this.width]);
    // this.y = this.d3.scaleLinear().range([this.height, 0]);
    // this.x.domain([0, 50]);
  }

  private initLine(): void {
    // TODO D3 service was removed
    // const xScale = this.x;
    // const yScale = this.y;
    // this.line = this.d3.line()
    //   .x(d => xScale(d[0]))
    //   .y(d => yScale(d[1]));
  }

  private drawAxis(): void {
    // TODO D3 service was removed
    // this.svg.append('g')
    //   .attr('class', 'x-axis')
    //   .attr('transform', 'translate(0,' + this.height + ')')
    //   .call(this.d3.axisBottom(this.x))
    //   .append('text')
    //   .attr('fill', '#000')
    //   .attr('x', this.width / 2)
    //   .attr('dy', '2.5em')
    //   .attr('text-anchor', 'middle')
    //   .text('Iteration');
    //
    // this.svg.append('g')
    //   .attr('class', 'y-axis')
    //   .call(this.d3.axisLeft(this.y))
    //   .append('text')
    //   .attr('fill', '#000')
    //   .attr('transform', 'rotate(-90)')
    //   .attr('y', 6)
    //   .attr('dy', '0.71em')
    //   .attr('text-anchor', 'end')
    //   .text('Cost');

  }

  private drawHorizontalLine(y: number) {

    this.svg.append('path')
      .attr('class', 'line')
      .datum([[0, y], [this.xMax, y]])
      .attr('fill', 'none')
      .attr('stroke', '#c51d1d')
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr('stroke-width', 1.5)
      .attr('d', this.line);
  }

  private drawLine(): void {
    this.svg.append('path')
      .attr('class', 'line')
      .datum(this.data)
      .attr('fill', 'none')
      .attr('stroke', 'steelblue')
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr('stroke-width', 1.5)
      .attr('d', this.line);
  }

  public setFinished(): void {
    this.drawHorizontalLine(this.data[this.data.length - 1][1]);
  }

  public updateAll(ys: number[]): void {
    // TODO D3 service was removed
    // ys.forEach(y => {
    //
    //   if (y > this.yMax) {
    //     this.yMax = y;
    //     this.y.domain([0, this.yMax]);
    //     this.d3.select('.y-axis')
    //       .call(this.d3.axisLeft(this.y));
    //   }
    //
    //   this.data.push([this.data.length, y]);
    // });
    //
    // this.x.domain([0, Math.min(this.xMax, Math.floor(this.data.length * 1.1))]);
    // this.d3.select('.x-axis')
    //   .call(this.d3.axisBottom(this.x));
    //
    // this.d3.select('.line').attr('d', this.line);
  }

  public update(y: number): void {
    // TODO D3 service was removed
    // if (y > this.yMax) {
    //   this.yMax = y;
    //   this.y.domain([0, this.yMax]);
    //   this.d3.select('.y-axis')
    //     .call(this.d3.axisLeft(this.y));
    // }
    //
    // const prevX = this.data.length;
    //
    // if (prevX > 40) {
    //   this.x.domain([0, Math.min(this.xMax, Math.floor(prevX * 1.1))]);
    //   this.d3.select('.x-axis')
    //     .call(this.d3.axisBottom(this.x));
    // }
    //
    //
    // this.data.push([prevX, y]);
    // this.d3.select('.line').attr('d', this.line);
  }
}
