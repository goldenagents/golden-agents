export interface ChartValue {
  x: number;
  y: number;
}
