import {Component, OnInit, Input, ElementRef} from '@angular/core';
// TODO D3 service was removed
// import {D3, D3Service, Selection} from 'd3-ng2-service';

@Component({
  selector: 'app-radial-bar',
  templateUrl: './radial-bar.component.html',
  styleUrls: ['./radial-bar.component.css']
})
export class RadialBarComponent implements OnInit {

  @Input()
  private dataFocus: number[];
  @Input()
  private dataCompare: number[];

  private minimum: number;
  private maximum: number;
  private minSize: number;
  private size: number;
  private centerOrigin: number;
  private centerRadius: number;
  private rectOriginX: number;
  private rectOriginY: number;
  private rectWidth: number;
  private scaleFactor: number;
  // TODO D3 service was removed
  // private d3: D3;
  // private svg: Selection<SVGSVGElement, any, null, undefined>;
  private center: any;
  private parentNativeElement;

  constructor(
    // private d3Service: D3Service,
    private element: ElementRef
  ) {
    this.parentNativeElement = element.nativeElement;
    // TODO D3 service was removed
    // this.d3 = d3Service.getD3();
    this.size = 1000;
    this.centerRadius = this.size / 100;
    this.centerOrigin = this.size / 2 - this.centerRadius;
    this.rectOriginX = this.centerOrigin + ((this.centerRadius) * Math.sin(0));
    this.rectOriginY = this.centerOrigin - ((this.centerRadius) * Math.cos(0));
    this.rectWidth = 6;
  }

  ngOnInit() {
    this.initSvg();
    if (this.dataCompare) {
      this.findRange(this.dataCompare);
    }
    if (this.dataFocus) {
      this.findRange(this.dataFocus);
    }
    if (this.dataCompare) {
      this.drawCompareData();
    }
    if (this.dataFocus) {
      this.drawFocusData();
    }
  }

  private initSvg(): void {
    // TODO D3 service was removed
    // const d3ParentElement = this.d3.select(this.parentNativeElement);
    // this.svg = d3ParentElement.select<SVGSVGElement>('svg#radialChart');
    // this.svg.attr('width', this.size);
    // this.svg.attr('height', this.size);
    // this.center = this.svg
    //   .append('circle')
    //   .attr('shape-rendering', 'geometricPrecision')
    //   .attr('r', this.centerRadius)
    //   .attr('cx', this.centerOrigin)
    //   .attr('cy', this.centerOrigin);
  }

  public setFocusData(data: number[]): void {
    this.findRange(data);
    this.dataFocus = data;
    this.drawFocusData();
  }

  public setCompareData(data: number[]): void {
    this.findRange(data);
    this.dataCompare = data;
    // TODO D3 service was removed
    // this.d3.selectAll('.compare-rect').remove();
    this.drawFocusData();
    this.drawCompareData();
  }

  private findRange(data: number[]): void {
    this.minimum = this.minimum == null ? data[0] : this.minimum;
    this.maximum = this.maximum == null ? data[0] : this.maximum;

    // Find the minimum and maximum
    data.forEach(d => {
      if (d < this.minimum) {
        this.minimum = d;
      }
      if (d > this.maximum) {
        this.maximum = d;
      }
    });

    this.minSize = 2 * Math.abs(this.minimum);
    // Make the largest value fit the size of the available space
    this.scaleFactor = (this.centerOrigin - this.centerRadius / 2) / (this.maximum + this.minSize);
  }

  private normalize(data: number[]): number[] {
    const norm = [...data];
    // Scale values so smallest (negative) value has positive sign
    data.forEach((d, i) => {
      norm[i] = data[i] + this.minSize;
    });
    return norm;
  }

  private drawCompareData(): void {
    this.normalize(this.dataCompare).forEach((d, i) => {
      const degree = 0.5 + (360 / this.dataCompare.length) * i;
      const height = d * this.scaleFactor;
      // TODO D3 service was removed
      // this.svg
      //   .append('rect')
      //   .attr('class', 'compare-rect')
      //   .style('fill', '#c51d1d')
      //   .style('opacity', 0.75)
      //   .attr('shape-rendering', 'geometricPrecision')
      //   .attr('x', this.rectOriginX - (this.rectWidth / 2))
      //   .attr('y', this.rectOriginY - (height))
      //   .attr('width', this.rectWidth)
      //   .attr('height', height)
      //   .attr('transform', 'rotate(' + degree + ', ' + this.centerOrigin + ', ' + this.centerOrigin + ')');

    });
  }

  private drawFocusData(): void {

    this.normalize(this.dataFocus).forEach((d, i) => {
      const degree = (360 / this.dataFocus.length) * i;
      const height = d * this.scaleFactor;
      // TODO D3 service was removed
      // this.svg
      //   .append('rect')
      //   .style('fill', 'steelblue')
      //   .attr('shape-rendering', 'geometricPrecision')
      //   .attr('x', this.rectOriginX - (this.rectWidth / 2))
      //   .attr('y', this.rectOriginY - (height))
      //   .attr('width', this.rectWidth)
      //   .attr('height', height)
      //   .attr('transform', 'rotate(' + degree + ', ' + this.centerOrigin + ', ' + this.centerOrigin + ')');

    });
  }
}
