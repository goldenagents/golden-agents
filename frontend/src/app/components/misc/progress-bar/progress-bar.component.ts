import {Component, OnInit, ViewEncapsulation} from '@angular/core';
// TODO D3 service was removed
// import {D3, D3Service} from 'd3-ng2-service';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {

  private margin = {top: 5, right: 5, bottom: 5, left: 5};
  private width: number;
  private height: number;
  private segmentWidth: number;
  private svg: any;
  private basebar: any;
  private progressbar: any;
  // TODO D3 service was removed
  // private d3: D3;

  constructor(
    // private d3Service: D3Service
  ) {
    // TODO D3 service was removed
    // this.d3 = d3Service.getD3();
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 25 - this.margin.top - this.margin.bottom;
    this.segmentWidth = this.width / 100;
  }

  ngOnInit(): void {
    this.initSvg();
    this.initBars();
  }

  private initSvg(): void {
    // TODO D3 service was removed
    // this.svg = this.d3.select('svg#progressBar')
    //   .append('g')
    //   .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private initBars(): void {

    this.basebar = this.svg.append('rect')
      .attr('class', 'bg-rect')
      .attr('rx', 10)
      .attr('ry', 10)
      .attr('height', this.height)
      .attr('width', this.width)
      .attr('fill', 'gray')
      .attr('x', 0);

    this.progressbar = this.svg.append('rect')
      .attr('class', 'progress-rect')
      .attr('fill', function() {
        return 'green';
      })
      .attr('height', this.height)
      .attr('width', 0)
      .attr('rx', 10)
      .attr('ry', 10)
      .attr('x', 0);
  }

  public setFinished(): void {
    this.update(100);
  }

  public update(percentage: number): void {
    const segmentWidth = this.segmentWidth;
    this.progressbar.transition()
      .duration(100)
      .style('fill', function() {
        return percentage === 100 ? 'green' : 'steelblue';
      })
      .attr('width', function() {
        return percentage * segmentWidth;
      });
  }
}
