// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { HttpHeaders } from '@angular/common/http';

export const environment = {
  production: false,
  HOST: window['env']['API']['HOST'] || 'http://localhost',
  PORT: window['env']['API']['PORT'] || 8080,
  BASE_URL: function(){return this.HOST + ':' + this.PORT + '/'; },
  BASE_API_URL: function(){return this.HOST + ':' + this.PORT + '/api/'; },
  VERSION: '0.0.1-alpha',
  HTTP_OPTIONS: {headers: new HttpHeaders({'Content-Type': 'application/json'})},
  HTTP_OPTIONS_PLAIN: {headers: new HttpHeaders({'Content-Type': 'text/plain; charset=utf-8'})}
};


