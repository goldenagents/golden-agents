import {HttpHeaders} from '@angular/common/http';

export const environment = {
  production: true,
  HOST: window['env']['API']['HOST'] || 'http://localhost',
  PORT: window['env']['API']['PORT'] || 8080,
  BASE_URL: function(){return this.HOST + ':' + this.PORT + '/'; },
  BASE_API_URL: function(){return this.HOST + ':' + this.PORT + '/api/'; },
  VERSION: '0.0.1-alpha',
  HTTP_OPTIONS: {headers: new HttpHeaders({'Content-Type': 'application/json'})},
  HTTP_OPTIONS_PLAIN: {headers: new HttpHeaders({'Content-Type': 'text/plain; charset=utf-8'})}
};
