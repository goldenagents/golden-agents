(function(window) {
  window['env'] = window['env'] || {};
  window['env']['API'] = window['env']['API'] || {};

  // Environment variables
  window['env']['API']['HOST'] = '${API_URL}';
  window['env']['API']['PORT'] = '${API_PORT}';
})(this);
