(function(window) {
  window['env'] = window['env'] || {};
  window['env']['API'] = window['env']['API'] || {};

  // Environment variables
  window['env']['API']['HOST'] = 'http://localhost';
  window['env']['API']['PORT'] = 8080;
})(this);
